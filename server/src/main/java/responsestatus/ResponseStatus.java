package responsestatus;

import java.util.HashMap;

public final class ResponseStatus {
	

		private String status = "";
		private String value = "";
		private String message="";
		HashMap<String, String> statusMap = new HashMap<>();
		
		
		public ResponseStatus(String status, String value){
			this.status = status;			
			this.value = value;	
		}
		public ResponseStatus(String status, String value,String msg){
			this(status, value);
			this.message=msg;
		}
		
		public HashMap<String, String> getStatus() {			
			this.statusMap.put("Status", this.status);			
			this.statusMap.put("Value", this.value);
			this.statusMap.put("Message", this.message);
			return statusMap;
		}

		 
	
}
