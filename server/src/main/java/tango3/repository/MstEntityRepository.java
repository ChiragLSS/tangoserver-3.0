package Tango3.repository;

import java.util.HashMap;
import java.util.List;

import javax.persistence.NamedNativeQuery;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Tango3.model.MstEntity;

@Repository
public interface MstEntityRepository extends GenericRepository<MstEntity> {

	List<MstEntity> findByNameIsNull();
	
	@Query("select new map(me.id as id,me.name as displayName,met.name as TypeName,met.id as TypeID) from MstEntity me join me.mstEntitytype met")
	public List<MstEntity> get();
	
	@Query("select new map(me.id as id,me.name as displayName,met.id as TypeID) from MstEntity me join me.mstEntitytype met where met.id =:typeID")
	public List<MstEntity> getByTypeID(@Param("typeID") int typeID);
	
	@Query("select new map("
			+ "pem.mstEntity.id as EntityID,"
			+ "pem.mstEntity.mstEntitytype.name as EntityTypeName,"
			+ "pem.mstEntity.name as EntityName,"
			+ "mpe.id as ParentEntityID,"
			+ "mpe.name as ParentEntityName,"
			+ "mpe.mstEntitytype.name  as ParentEntityTypeName,"
			+ "concat(pmu.wikiName,'(',pmr.roleName,')') as ParentUserNameWithRole"
			+ ") from MstEntity me "
			
			+ "left join me.parentEntityMappings pem "
			+ "left join pem.mstParentEntity mpe "
			+ "left join mpe.entitymembermappings pemm "
			+ "left join pemm.mstMember pmm "
			+ "left join pmm.mstRole pmr "
			+ "left join pmm.mstUserdetail pmu "
			+ " where me.id=:id "
			+ " and me.isActive = 1 "
			+ "and pmm.isActive = 1"
			)
	public List<HashMap<String, String>> getChildList(@Param("id") int id);
	
	@Query("select new map("
			+ "mpe.id as ParentEntityID,"
			+ "mpe.mstEntitytype.name as ParentEntityTypeName,"
			+ "mpe.mstEntitytype.id as ParentEntityTypeID,"
			+ "mpe.name as ParentEntityName,"
			+ "me.name as EntityName,"
			+ "me.id as EntityID,"
			+ "me.mstEntitytype.name as EntityTypeName,"
			+ "concat(mu.wikiName,'(',mr.roleName,')') as ParentEntityUserNameWithRole,"
			+ "mu.id as ParentEntityUserID"
			+ ") From MstEntity me "
			+ " left join me.entityMappings em "
			+ " left join em.mstParentEntity mpe "
			+ " left join mpe.entitymembermappings emm "
			+ " left join emm.mstMember mm "
			+ " left join mm.mstRole mr "
			+ " left join mm.mstUserdetail mu "
			+ "where me.id = :id "
			+ "and mm.isActive = 1 "
			+ "and me.isActive = 1")
	public List<HashMap<String, String>> getParentList(@Param("id") int id);

	@Query("select me from MstEntity me where me.id =:entityID and me.mstEntitytype.id=:typeID and me.isActive = :active")
	public MstEntity checkEntityExist(@Param("entityID") int entityID,@Param("typeID") int typeID,@Param("active") byte active);
	
	
	public MstEntity findById(int id);
	
	@Query("select me.id from MstEntity me where me.name=:entityName")
	public int getEntityIdByEntityName(@Param("entityName")String entityName);
	
	
}
