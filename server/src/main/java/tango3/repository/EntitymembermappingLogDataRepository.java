package Tango3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.EntitymembermappingLogData;

public interface EntitymembermappingLogDataRepository extends GenericRepository<EntitymembermappingLogData> {

	
	public EntitymembermappingLogData findById(int id);
	
	@Query("select new map(emmld.id as ID, "
			+ "emmld.mstEntity.name as EntityName,"
			+ "emmld.mstMember.mstRole.roleName as RoleName,"
			+ "emmld.startDate as StartDate,"
			+ "emmld.endDate  as EndDate,"
			+ "CASE WHEN emmld.endDate IS NULL THEN "
			+ "DATEDIFF(current_date,emmld.startDate) ELSE "
			+ "DATEDIFF(emmld.endDate,emmld.startDate) END as noOfDays) from EntitymembermappingLogData emmld where emmld.mstMember.mstUserdetail.id = :id")
	public List getTransitionRecord(@Param("id") int id);
	
	
	
}
