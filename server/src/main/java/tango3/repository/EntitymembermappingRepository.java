package Tango3.repository;

import java.util.HashMap;
import java.util.List;

import org.hibernate.annotations.AttributeAccessor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.Entitymembermapping;
import Tango3.model.MstEntity;
import Tango3.util.UtilConstants;
import net.bytebuddy.build.HashCodeAndEqualsPlugin;

public interface EntitymembermappingRepository extends GenericRepository<Entitymembermapping> {

	
	@Query("select new map("
			+ "me.id as EntityID,"
			+ "me.name as EntityName,"
			+ "me.mstEntitytype.name as EntityTypeName,"
			+ "mu.wikiName as userName,"
			+ "mr.roleName as entityRoleName,"
			+ "mpe.mstEntitytype.name  as  ParentEntityTypeName,"
			+ "mpe.id as ParentEntityID,"
			+ "mpe.name as ParentEntityName,"
			+ "concat(pmu.wikiName,'(',pmr.roleName,')') as ParentEntityUserNameWithRole,"
			+ "pmu.id as ParentEntityUserID,"
			+ "mu.id as EntityUserID"
			+ ") From Entitymembermapping e "
			+ " join e.mstEntity me "
			+ " left join me.entityMappings em "
			+ " left join em.mstParentEntity mpe "
			+ " left join mpe.entitymembermappings emm "
			+ " left join emm.mstMember pmm "
			+ " join pmm.mstRole pmr "
			+ " join pmm.mstUserdetail pmu "
			+ " join e.mstMember mm "
			+ " join mm.mstRole mr "
			+ " join mm.mstUserdetail mu "
			+ "where me.id = :mstEntity "
			+ "and mm.isActive = 1")
	public List<HashMap<String, String>> getByEntityID(@Param("mstEntity") int mstEntity);
	
	@Query("select new map("
			+ "e.id as memberMappingID,"
			+ "em.id as entityID,"
			+ "em.name as entityName,"
			+ "mr.roleName as roleName,"
			+ "mu.userID as userID," 
			+ "mu.wikiName as userName"
			+ ") From Entitymembermapping e "
			+ " join e.mstEntity em "
			+ " join e.mstMember mm "
			+ " join mm.mstRole mr "
			+ " join mm.mstUserdetail mu ")
	public List getTlGlPmMember();
	
	@Query("select count(e) From Entitymembermapping e "
			+ " join e.mstEntity em "
			+ " join e.mstMember mm "
			+ " join mm.mstRole mr "
			+ " join mm.mstUserdetail mu ")
	public int getCountForAllTlGlPmMember();
	
	
	@Query("select new map("
			+ "me.name as ParentEntityName,"
			+ "me.id as ParentEntityID,"
			+ "me.mstEntitytype.name as ParentEntityTypeName,"
			+ "me.mstEntitytype.id as ParentEntityTypeID,"
			+ "mr.roleName as EntityRoleName,"
			+ "mu.wikiName as EntityName,"
			+ "mu.id as userID"
			+ ") from Entitymembermapping emm "
			+ " left join emm.mstEntity me "
			+ " left join emm.mstMember mm "
			+ " left join mm.mstRole mr "
			+ " left join mm.mstUserdetail mu "
			+ " where mu.id = :id ")
	public List<HashMap<String, String>> getParentDataByUserID(@Param("id") int id);
	
	@Query("select new map("
			+ "me.name as ParentEntityName,"
			+ "me.id as ParentEntityID,"
			+ "me.mstEntitytype.name as ParentEntityTypeName,"
			+ "me.mstEntitytype.id as ParentEntityTypeID,"
			+ "mr.roleName as EntityRoleName,"
			+ "mu.wikiName as EntityName,"
			+ "mm.id as memberId,"
			+ "mu.id as userID"
			+ ") from Entitymembermapping emm "
			+ " left join emm.mstEntity me "
			+ " left join emm.mstMember mm "
			+ " left join mm.mstRole mr "
			+ " left join mm.mstUserdetail mu "
			+ " where me.id = :id ")
	public List<HashMap<String, String>> getParentDataByEntityID(@Param("id") int id);

	@Query("select emm from Entitymembermapping emm where emm.mstEntity.id =:entityID and emm.mstEntity.mstEntitytype.id=:typeID and emm.mstMember.isActive = :active and emm.mstMember.id = :memberID")
	public Entitymembermapping checkMemberExist(@Param("entityID") int entityID,@Param("typeID") int typeID,@Param("memberID") int memberID,@Param("active") byte isActive);
	
	@Query("select emm from Entitymembermapping emm where emm.mstMember.id = :id")
	public Entitymembermapping findByMemberID(@Param("id")int id);
	
	@Query("select new map(emm.mstEntity.id as EntityID,"
			+ "emm.mstEntity.name as EntityName,"
			+ "emm.mstEntity.mstEntitytype.id as EntityTypeID)"
			+ " from Entitymembermapping emm "
			+ "where emm.mstMember.mstUserdetail.id =:userID "
			+ "and emm.mstMember.isActive = 1 and "
			+ "emm.mstEntity.mstEntitytype.id=("
			+ " select MIN(emmp.mstEntity.mstEntitytype.id) " 	 
			+ " from Entitymembermapping emmp " 
			+ " where emmp.mstMember.mstUserdetail.id =:userID "  
			+ " and emmp.mstMember.isActive = 1"
			+ ")")
	public List getBelongToEntityIDByUserID(@Param("userID")int userID);
	
	
	@Query("select new map(pme.mstParentEntity.id as EntityID,"
			+ "pme.mstParentEntity.name as EntityName,"
			+ "pme.mstParentEntity.mstEntitytype.id as EntityTypeID)"
			+ " from Entitymembermapping emm "
			+ " inner join emm.mstEntity me "
			+ " left join me.entityMappings pme "
			+ " where emm.mstMember.mstUserdetail.id =:userID "
			+ "and emm.mstMember.isActive = 1 ")
	public List getReportsToEntityIDByUserID(@Param("userID")int userID);
	
	
	@Query("select new map(emm.mstEntity.name as EntityName,emm.mstMember.mstUserdetail.id as UserID) from Entitymembermapping emm "
			+ "where emm.mstMember.mstRole.id =3 ")
	public List getMemberDeteilFromEntity();
	
	
	
	
}
