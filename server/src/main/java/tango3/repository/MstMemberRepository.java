package Tango3.repository;

import java.util.HashMap;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.MstMember;

public interface MstMemberRepository extends GenericRepository<MstMember>{

	
	public MstMember findById(int id);
	
	
	@Query("select new map(mm.id as memberID) from MstMember mm where mm.mstUserdetail.id =:id and mm.isActive = 1")
	public List<HashMap<String, Integer>> getListOfMemberIDFromUserID(@Param("id") int id);
	
	
	@Query("select new map("
			+ "mm.id as MemberID,"
			+ "mu.id as userID,"
			+ "mu.wikiName as userName,"
			+ "mu.email as email,"
			+ "mu.employementType as employementType,"
			+ "mu.employeeStatus as EmployeeStatus,"
			+ "function('date_format',mu.joinDate,'%Y/%m/%d %H:%i:%s') as joinDate,"
			+ "function('date_format',mu.leavingDate,'%Y/%m/%d %H:%i:%s') as LeavingDate" 
			+ ") " 
			+ "from MstMember mm "
			+ "join mm.mstUserdetail mu "
			+ "where mu.id != :userID "
			+ "and mm.id = :memberID ")
	public List getUserDetailFromMemberID(@Param("memberID") int memberID,@Param("userID") int userID);
 
	@Query("select new map(mm.mstUserdetail.id as UserID) from MstMember mm where mm.id = :id")
	public HashMap<String, Integer> getUserIDFromMemberID(@Param("id") int id);
	
	@Query("select new map(mm.mstRole.roleName) From MstMember mm where mm.id =:id and mm.mstRole.id = 3")
	public HashMap<String, String> checkisMemberLevel(@Param("id") int id);
	
	
	@Query("select  mm From MstMember mm where mm.isActive = 1")
	public List getAllActiveMember();
}
