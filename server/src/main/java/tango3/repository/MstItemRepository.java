package Tango3.repository;

import java.util.HashMap;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.MstItem;

public interface MstItemRepository extends GenericRepository<MstItem> {

	
	@Query(value="Select new map("
			+ "MI.itemID as itemID,"
			+ "MI.itemName as itemName,"
			+ "MIT.id as id,"
			+ "MIT.name as name,"
			+ "PMI.itemID as parentID,"
			+ "PMI.itemName as parentName,"
			+ "PMIT.id as parentItemType,"
			+ "PMIT.name as parentItemTypeName,"
			+ "TC.isActive as childIsActive,"
			+ "PTC.isActive as parentIsActive "
			+ ") "
			+ "From MstItem MI "
			+ "left join MI.mstItemtype MIT "
			+ "left join MI.tangocomponent TC "
			+ "left join MI.mstItem PMI "
			+ "left join PMI.mstItemtype PMIT "
			+ "left join PMI.tangocomponent PTC ")
	public List getItemParentMappingList();
	
	
	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where mi.mstItemtype.id=:itemtypeID AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id IS NULL AND parent.mstRole.roleID IS NULL ")
	public List<HashMap<String, String>> getGetRightsDetail1(@Param("itemtypeID")int itemtypeID,
												@Param("entitytypeID")int entitytypeID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where mi.mstItemtype.id =:itemtypeID AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL")
	public List<HashMap<String, String>> getGetRightsDetail2(@Param("itemtypeID")int itemtypeID,@Param("entitytypeID")int entitytypeID,@Param("entityID")int entityID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where mi.mstItemtype.id =:itemtypeID AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID")
	public List<HashMap<String, String>> getGetRightsDetail3(@Param("itemtypeID")int itemtypeID,@Param("entitytypeID")int entitytypeID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID")
	public List<HashMap<String, String>> getGetRightsDetail4(@Param("itemtypeID")int itemtypeID,@Param("entitytypeID")int entitytypeID,@Param("entityID")int entityID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID AND parent.mstRole.roleID IS NULL")
	public List<HashMap<String, String>> getGetRightsDetail5(@Param("itemtypeID")int itemtypeID,@Param("entityID")int entityID);

//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID "
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID")
	public List<HashMap<String, String>> getGetRightsDetail6(@Param("itemtypeID")int itemtypeID,@Param("entityID")int entityID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID")
	public List<HashMap<String, String>> getGetRightsDetail7(@Param("itemtypeID")int itemtypeID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID "
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID ")
	public List<HashMap<String, String>> getGetRightsDetail8(@Param("itemtypeID")int itemtypeID,@Param("entityID")int entityID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL "
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL ")
	public List<HashMap<String, String>> getGetRightsDetail9(@Param("itemtypeID")int itemtypeID,@Param("entityID")int entityID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL "	
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID IS NULL ")
	public List<HashMap<String, String>> getGetRightsDetail10(@Param("itemtypeID")int itemtypeID,@Param("entitytypeID")int entitytypeID,@Param("entityID")int entityID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID "
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id =:entitytypeID AND parent.mstEntity.id =:entityID  AND parent.mstRole.roleID =:roleID ")
	public List<HashMap<String, String>> getGetRightsDetail11(@Param("itemtypeID")int itemtypeID,@Param("entitytypeID")int entitytypeID,@Param("entityID")int entityID,@Param("roleID")int roleID);
	
//	+ "OR (mi.mstItemtype.id =:itemtypeID OR mi.mstItemtype.id = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID "
	@Query(value=  "select new map(mi.mstItem.itemID as parentItemID,mi.itemID as itemID,mi.itemName as itemName) from MstItem mi LEFT JOIN mi.allrightsdetailsParent parent "
			+ "where (mi.mstItemtype.id =:itemtypeID OR :itemtypeID = -1) AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID =:roleID ")
	public List<HashMap<String, String>> getGetRightsDetail12(@Param("itemtypeID")int itemtypeID,@Param("roleID")int roleID);
	
}
