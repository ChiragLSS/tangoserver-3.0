package Tango3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.MstUserdetail;

public interface MstUserdetailRepository extends GenericRepository<MstUserdetail>{

	
	@Query("select mu From MstUserdetail mu")
	public List getAllUserDetail();
	
	public MstUserdetail findByUserID(int userID);
	
	@Query("select new map(mu.degree as degree) from MstUserdetail mu  where mu.degree != Null and mu.degree != '' Group by mu.degree")
	public List getDegreeListForNewUser();
	
	@Query("select new map(mu.branch as branch) from MstUserdetail mu  where mu.branch != Null and mu.branch != '' Group by mu.branch")
	public List getBranchListForNewUser();
	
	@Query("select new map(mu.id as ID) From MstUserdetail mu where mu.wikiName =:wikiName")
	public List checkUserNameisExistsForNewUser(@Param("wikiName") String wikiName);
	
	@Query("select new map(mu.wikiName as userName,mu.userID,mu.email,mu.employementType,mu.employeeStatus,mu.joinDate,mu.leavingDate) From MstUserdetail mu")
	public List listOFAllUsers();
	
	@Query("select new map(mu.wikiName as userName,mu.userID,mu.email,mu.employementType,mu.employeeStatus,mu.joinDate,mu.leavingDate) From MstUserdetail mu where mu.isActive = 1")
	public List activeUsers();
	
	@Query(value="select mu.wiki_Name as wikiName,mu.email as email,mu.degree as degree,mu.branch as branch,mu.gender as gender,mu.graduation_Year as graduationYear,mu.last_College as lastCollege,group_concat(mcu.contactNumber,'') as contactNumber From mst_userdetail mu Inner join mst_usercontactdetail mcu on mcu.userID = mu.userID where mu.userID =:userID AND mcu.contactPerson = 'Own' ",nativeQuery=true)
	public List getUserProfileSummary(@Param("userID") int userID);
	
	@Query(value="select new map(mu.employementType as employementType,mu.firstName as firstName,mu.lastName as lastName,mu.personalEmail as personalEmail,mu.birthDate as birthDate,mcu.contactNumber as contactNumber,mcu.contactPerson as contactPerson,mcu.relationship as relationship,mu.isActive as isActive,mu.joinDate as joinDate) From MstUserdetail mu join mu.mstUsercontactdetails mcu where mu.userID =:userID")
	public List getUserPersonalDetail(@Param("userID") int userID);
	
	
	@Query("Select new map(mu.firstName as firstName,mu.lastName as lastName,mu.employeeStatus as employeeStatus,mu.profileImagePath as profileImagePath,mu.gender as gender,mu.joinDate as joinDate,"
			+ "CASE WHEN mu.leavingDate IS NULL THEN "
			+ "DATEDIFF(current_date,mu.joinDate) ELSE "
			+ "DATEDIFF(mu.leavingDate,mu.joinDate) END as noOfDays) From MstUserdetail mu where mu.userID =:userID")
	public List getUserProfilePicWithBasicDetails(@Param("userID") int userID);

	@Query("select concat(mu.firstName,mu.lastName) From MstUserdetail mu where mu.userID =:userID")
	public String getUserNameFromUserID(@Param("userID") int userID);
	
	@Query("select mu.isGm From MstUserdetail mu where mu.userID =:userID")
	public byte isUserGM(@Param("userID") int userID);
	
	@Query("select mu.isSuperUser From MstUserdetail mu where mu.userID =:userID")
	public byte isSuperUser(@Param("userID") int userID);
	
	@Query("select mu.userID from MstUserdetail mu where mu.wikiName =:wikiName")
	public int getUserIDFromWikiName(@Param("wikiName") String wikiName);
	
	@Query("Select mu.leavingDate From MstUserdetail mu where mu.userID =:userID")
	public String getLeavingDate(@Param("userID") int userID);
	
	
	@Query("Select mu.employementType From MstUserdetail mu where mu.userID =:userID")
	public String getEmployementType(@Param("userID") int userID);
	
	@Query("Select mu From MstUserdetail mu where mu.firstName LIKE concat('%',:firstName,'%') AND mu.lastName LIKE concat('%',:lastName,'%') AND mu.leavingDate IS NULL AND mu.isActive = 1 ")
	public List<MstUserdetail> checkUserExist(@Param("firstName") String firstName,@Param("lastName") String lastName);
	
	@Query("Select mu From MstUserdetail mu where mu.email=:email")
	public List<MstUserdetail> getUserDetailFromEmail(@Param("email") String email);
	
}
