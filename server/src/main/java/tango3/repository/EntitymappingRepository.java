package Tango3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import Tango3.model.Entitymapping;

public interface EntitymappingRepository extends GenericRepository<Entitymapping> {

	
	@Query("select distinct e from Entitymapping e ")
	public List<Entitymapping> get();
	
	
//	@Query("select new map(mpe.name as ParentEntityName,me.name as EntityName,mu.wikiName as EntityUserName,mr.roleName as EntityRoleName,parentU.wikiName as ParentEntityUserName,parentR.roleName as ParentEntityRoleName)"
//			+ "From Entitymapping em "
//			+ "left join em.mstParentEntity mpe "
//			+ "left join em.mstEntity me "
//			+ "left join me.entitymembermappings emm "
//			+ "left join emm.mstMember mm "
//			+ "left join mm.mstRole mr "
//			+ "left join mm.mstUserdetail mu "
//			+ "left join mpe.entitymembermappings parentE "
//			+ "left join parentE.mstMember parentM "
//			+ "left join parentM.mstRole parentR "
//			+ "left join parentM.mstUserdetail parentU "
//			+ "where 1=1 " 
//			+ "or me.id=:entityID "
//			+ "or mpe.id=:parentEntityID")
//	public List getChildList(@Param("entityID") int entityID,@Param("parentEntityID")int parentEntityID);
//	
	
	
	
	
	
}
