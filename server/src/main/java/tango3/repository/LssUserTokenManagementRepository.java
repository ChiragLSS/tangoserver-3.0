package Tango3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.LssUserTokenManagement;

public interface LssUserTokenManagementRepository extends GenericRepository<LssUserTokenManagement>{

	
	
	@Query("Select new map("
			+ "concat(mu.firstName,mu.lastName) as entityName,"
			+ "mu.userID as userID,"
			+ "mu.email as email,"
			+ "mu.joinDate as joinDate,"
			+ "mu.leavingDate as leavingDate,"
			+ "mu.isActive as isActive,"
			+ "mu.employeeStatus as employeeStatus,"
			+ "mu.isTangoAdmin as isTangoAdmin,"
			+ "mu.employementType as employementType,"
			+ "mu.isGm as isGm,"
			+ "mu.isSuperUser as isSuperUser,"
			+ "mu.gender as gender,"
			+ "mu.profileImagePath as profileImagePath,"
			+ "elm.id as entityId,"
			+ "met.id as typeID,"
			+ "met.name as typeName,"
			+ "mr.roleID as roleID,"
			+ "mr.roleName as roleName,"
			+ "CASE WHEN em.mstEntity.id IS NOT NULL THEN "
			+ "em.mstEntity.id ELSE "
			+ "elm.mstEntity.id END as parentID,"
			+ "CASE WHEN em.mstEntity.id IS NOT NULL THEN "
			+ "me.name ELSE "
			+ "me.name END as parentName" 
			+ ") From LssUserTokenManagement LUTM "
			+ " LEFT JOIN LUTM.mstUserdetail mu "
			+ " LEFT JOIN mu.mstMembers mm "
			+ " LEFT JOIN mm.entitymembermappings elm "
			+ " LEFT JOIN elm.mstEntity me "
			+ " LEFT JOIN me.mstEntitytype met "
			+ " LEFT JOIN mm.mstRole mr "
			+ " LEFT JOIN me.entityMappings em "
			+ " LEFT JOIN em.mstParentEntity mpe "
			+ "WHERE LUTM.token=:token")
	public List getUserDetailFromToken(@Param("token") String token);
	
}
