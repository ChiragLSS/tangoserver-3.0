package Tango3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import Tango3.model.Tangocomponent;

public interface TangocomponentRepository extends GenericRepository<Tangocomponent> {

//	@Query("select new map("
//			+ ""
//			+ ")"
//			+ "From Tangocomponent tc "
//			+ "left join tc.mstItems mi "
//			+ "left join ")
//	public List getParentChildItem(@Param("itemID") int itemID,@Param("parentItemID") int parentItemID);
//		
}
