package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_member database table.
 * 
 */
@Entity
@Table(name="mst_member")
@NamedQuery(name="MstMember.findAll", query="SELECT m FROM MstMember m")
public class MstMember implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date create_Ts;

	@Column(name="create_user_name")
	private String createUserName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="is_active")
	private byte isActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modify_Ts;

	@Column(name="modify_user_name")
	private String modifyUserName;

	@ManyToOne
	@JoinColumn(name="role_id")
	private MstRole mstRole;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to Entitymembermapping
	@OneToMany(mappedBy="mstMember")
	@JsonBackReference
	private List<Entitymembermapping> entitymembermappings;

	@OneToMany(mappedBy="mstMember")
	@JsonBackReference
	private List<EntitymembermappingLogData> entitymembermappingLogData;
	
	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="user_id")
	private MstUserdetail mstUserdetail;

	public MstMember() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreate_Ts() {
		return this.create_Ts;
	}

	public void setCreate_Ts(Date create_Ts) {
		this.create_Ts = create_Ts;
	}

	public String getCreateUserName() {
		return this.createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public Date getModify_Ts() {
		return this.modify_Ts;
	}

	public void setModify_Ts(Date modify_Ts) {
		this.modify_Ts = modify_Ts;
	}

	public String getModifyUserName() {
		return this.modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	
	public MstRole getMstRole() {
		return mstRole;
	}

	public void setMstRole(MstRole mstRole) {
		this.mstRole = mstRole;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Entitymembermapping> getEntitymembermappings() {
		return this.entitymembermappings;
	}

	public void setEntitymembermappings(List<Entitymembermapping> entitymembermappings) {
		this.entitymembermappings = entitymembermappings;
	}

	public Entitymembermapping addEntitymembermapping(Entitymembermapping entitymembermapping) {
		getEntitymembermappings().add(entitymembermapping);
		entitymembermapping.setMstMember(this);

		return entitymembermapping;
	}

	public Entitymembermapping removeEntitymembermapping(Entitymembermapping entitymembermapping) {
		getEntitymembermappings().remove(entitymembermapping);
		entitymembermapping.setMstMember(null);

		return entitymembermapping;
	}

	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}
	
	public List<EntitymembermappingLogData> getEntitymembermappingLogData() {
		return this.entitymembermappingLogData;
	}

	public void setEntitymembermappingLogData(List<EntitymembermappingLogData> entitymembermappingLogData) {
		this.entitymembermappingLogData = entitymembermappingLogData;
	}

	public EntitymembermappingLogData addEntitymembermappingLogData(EntitymembermappingLogData entitymembermappingLogData) {
		getEntitymembermappingLogData().add(entitymembermappingLogData);
		entitymembermappingLogData.setMstMember(this);

		return entitymembermappingLogData;
	}

	public EntitymembermappingLogData removeEntitymembermappingLogData(EntitymembermappingLogData entitymembermappingLogData) {
		getEntitymembermappingLogData().remove(entitymembermappingLogData);
		entitymembermappingLogData.setMstMember(null);

		return entitymembermappingLogData;
	}

}