package Tango3.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the permissions database table.
 * 
 */
@Entity
@Table(name="permissions")
@NamedQuery(name="Permission.findAll", query="SELECT p FROM Permission p")
public class Permission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int permissionID;

	@Column(name="permissionname")
	private String permissionName;

	//bi-directional many-to-one association to Itempermissionmapping
	@OneToMany(mappedBy="permission")
	@JsonBackReference
	private List<Itempermissionmapping> itempermissionmappings;

	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="permission")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	
	public Permission() {
	}

	public int getPermissionID() {
		return this.permissionID;
	}

	public void setPermissionID(int permissionID) {
		this.permissionID = permissionID;
	}

	public String getPermissionName() {
		return this.permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public List<Itempermissionmapping> getItempermissionmappings() {
		return this.itempermissionmappings;
	}

	public void setItempermissionmappings(List<Itempermissionmapping> itempermissionmappings) {
		this.itempermissionmappings = itempermissionmappings;
	}

	public Itempermissionmapping addItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		getItempermissionmappings().add(itempermissionmapping);
		itempermissionmapping.setPermission(this);

		return itempermissionmapping;
	}

	public Itempermissionmapping removeItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		getItempermissionmappings().remove(itempermissionmapping);
		itempermissionmapping.setPermission(null);

		return itempermissionmapping;
	}

	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setPermission(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setPermission(null);

		return allrightsdetail;
	}

}