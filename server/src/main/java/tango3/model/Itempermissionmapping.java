package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the itempermissionmapping database table.
 * 
 */
@Entity
@Table(name="itempermissionmapping")
@NamedQuery(name="Itempermissionmapping.findAll", query="SELECT i FROM Itempermissionmapping i")
public class Itempermissionmapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int mappingID;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;
	
	@Column(name="jiratask")
	private int jiraTask;

	private String link;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstItem
	@ManyToOne
	@JoinColumn(name="itemID")
	private MstItem mstItem;

	//bi-directional many-to-one association to Permission
	@ManyToOne
	@JoinColumn(name="permissionID")
	private Permission permission;

	//bi-directional many-to-one association to Itemright
	@OneToMany(mappedBy="itempermissionmapping")
	@JsonBackReference
	private List<Itemright> itemrights;
	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="itempermissionmapping")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;


	public Itempermissionmapping() {
	}

	public int getMappingID() {
		return this.mappingID;
	}

	public void setMappingID(int mappingID) {
		this.mappingID = mappingID;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	

	public int getJiraTask() {
		return this.jiraTask;
	}

	public void setJiraTask(int jiraTask) {
		this.jiraTask = jiraTask;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstItem getMstItem() {
		return this.mstItem;
	}

	public void setMstItem(MstItem mstItem) {
		this.mstItem = mstItem;
	}

	public Permission getPermission() {
		return this.permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public List<Itemright> getItemrights() {
		return this.itemrights;
	}

	public void setItemrights(List<Itemright> itemrights) {
		this.itemrights = itemrights;
	}

	public Itemright addItemright(Itemright itemright) {
		getItemrights().add(itemright);
		itemright.setItempermissionmapping(this);

		return itemright;
	}

	public Itemright removeItemright(Itemright itemright) {
		getItemrights().remove(itemright);
		itemright.setItempermissionmapping(null);

		return itemright;
	}

	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setItempermissionmapping(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setItempermissionmapping(null);

		return allrightsdetail;
	}

	
}