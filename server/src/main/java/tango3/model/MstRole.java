package Tango3.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_role database table.
 * 
 */
@Entity
@Table(name="mst_role")
@NamedQuery(name="MstRole.findAll", query="SELECT m FROM MstRole m")
public class MstRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int roleID;

	private String description;

	private int isActive;

	private int level;

	private String roleName;

	//bi-directional many-to-one association to MstMember
	@OneToMany(mappedBy="mstRole")
	@JsonBackReference
	private List<MstMember> mstMembers;
	
	//bi-directional many-to-one association to Itemright
	@OneToMany(mappedBy="mstRole")
	@JsonBackReference
	private List<Itemright> itemrights;
	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstRole")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;



	public MstRole() {
	}

	public int getRoleID() {
		return this.roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIsActive() {
		return this.isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<MstMember> getMstMembers() {
		return this.mstMembers;
	}

	public void setMstMembers(List<MstMember> mstMembers) {
		this.mstMembers = mstMembers;
	}

	public MstMember addMstMember(MstMember mstMember) {
		getMstMembers().add(mstMember);
		mstMember.setMstRole(this);

		return mstMember;
	}

	public MstMember removeMstMember(MstMember mstMember) {
		getMstMembers().remove(mstMember);
		mstMember.setMstRole(null);

		return mstMember;
	}

	public List<Itemright> getItemrights() {
		return this.itemrights;
	}

	public void setItemrights(List<Itemright> itemrights) {
		this.itemrights = itemrights;
	}

	public Itemright addItemright(Itemright itemright) {
		getItemrights().add(itemright);
		itemright.setMstRole(this);

		return itemright;
	}

	public Itemright removeItemright(Itemright itemright) {
		getItemrights().remove(itemright);
		itemright.setMstRole(null);

		return itemright;
	}
	
	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setMstRole(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setMstRole(null);

		return allrightsdetail;
	}

}