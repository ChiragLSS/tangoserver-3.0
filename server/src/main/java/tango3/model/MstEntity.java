package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_entity database table.
 * 
 */
@Entity
@Table(name="mst_entity")
@NamedQuery(name="MstEntity.findAll", query="SELECT m FROM MstEntity m")
public class MstEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date create_Ts;

	@Column(name="create_user_name")
	private String createUserName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="is_active")
	private byte isActive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modify_Ts;

	@Column(name="modify_user_name")
	private String modifyUserName;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to Entitymapping
	@OneToMany(mappedBy="mstEntity")
	@JsonBackReference
	private List<Entitymapping> entityMappings;

	//bi-directional many-to-one association to Entitymapping
	@OneToMany(mappedBy="mstParentEntity")
	@JsonBackReference
	private List<Entitymapping> parentEntityMappings;

	//bi-directional many-to-one association to Entitymembermapping
	@OneToMany(mappedBy="mstEntity")
	@JsonBackReference
	private List<Entitymembermapping> entitymembermappings;

	//bi-directional many-to-one association to MstEntitytype
	@ManyToOne
	@JoinColumn(name="type_id")
	private MstEntitytype mstEntitytype;

	@OneToMany(mappedBy="mstEntity")
	@JsonBackReference
	private List<EntitymembermappingLogData> entitymembermappingLogData;
	
	//bi-directional many-to-one association to Itemright
	@OneToMany(mappedBy="mstEntity")
	private List<Itemright> itemrights;
	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstEntity")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	
	public MstEntity() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreate_Ts() {
		return this.create_Ts;
	}

	public void setCreate_Ts(Date create_Ts) {
		this.create_Ts = create_Ts;
	}

	public String getCreateUserName() {
		return this.createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public Date getModify_Ts() {
		return this.modify_Ts;
	}

	public void setModify_Ts(Date modify_Ts) {
		this.modify_Ts = modify_Ts;
	}

	public String getModifyUserName() {
		return this.modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Entitymapping> getEntityMappings() {
		return this.entityMappings;
	}

	public void setEntityMappings(List<Entitymapping> entitymappings) {
		this.entityMappings = entitymappings;
	}

	public Entitymapping addEntitymappings1(Entitymapping entityMappings) {
		getEntityMappings().add(entityMappings);
		entityMappings.setMstEntity(this);

		return entityMappings;
	}

	public Entitymapping removeEntityMappings(Entitymapping entityMappings) {
		getEntityMappings().remove(entityMappings);
		entityMappings.setMstEntity(null);

		return entityMappings;
	}

	public List<Entitymapping> getParentEntityMappings() {
		return this.parentEntityMappings;
	}

	public void setParentEntityMappings(List<Entitymapping> parentEntityMappings) {
		this.parentEntityMappings = parentEntityMappings;
	}

	public Entitymapping addParentEntityMappings(Entitymapping parentEntityMappings) {
		getParentEntityMappings().add(parentEntityMappings);
		parentEntityMappings.setMstEntity(this);

		return parentEntityMappings;
	}

	public Entitymapping removeParentEntityMappings(Entitymapping parentEntityMappings) {
		getParentEntityMappings().remove(parentEntityMappings);
		parentEntityMappings.setMstEntity(null);

		return parentEntityMappings;
	}

	public List<Entitymembermapping> getEntitymembermappings() {
		return this.entitymembermappings;
	}

	public void setEntitymembermappings(List<Entitymembermapping> entitymembermappings) {
		this.entitymembermappings = entitymembermappings;
	}

	public Entitymembermapping addEntitymembermapping(Entitymembermapping entitymembermapping) {
		getEntitymembermappings().add(entitymembermapping);
		entitymembermapping.setMstEntity(this);

		return entitymembermapping;
	}

	public Entitymembermapping removeEntitymembermapping(Entitymembermapping entitymembermapping) {
		getEntitymembermappings().remove(entitymembermapping);
		entitymembermapping.setMstEntity(null);

		return entitymembermapping;
	}

	public MstEntitytype getMstEntityType() {
		return this.mstEntitytype;
	}

	public void setMstEntityType(MstEntitytype mstEntityType) {
		this.mstEntitytype = mstEntityType;
	}
	public List<EntitymembermappingLogData> getEntitymembermappingLogData() {
		return this.entitymembermappingLogData;
	}

	public void setEntitymembermappingLogData(List<EntitymembermappingLogData> entitymembermappingLogData) {
		this.entitymembermappingLogData = entitymembermappingLogData;
	}

	public EntitymembermappingLogData addEntitymembermappingLogData(EntitymembermappingLogData entitymembermappingLogData) {
		getEntitymembermappingLogData().add(entitymembermappingLogData);
		entitymembermappingLogData.setMstEntity(this);

		return entitymembermappingLogData;
	}

	public EntitymembermappingLogData removeEntitymembermappingLogData(EntitymembermappingLogData entitymembermappingLogData) {
		getEntitymembermappingLogData().remove(entitymembermappingLogData);
		entitymembermappingLogData.setMstEntity(null);

		return entitymembermappingLogData;
	}
	
	public List<Itemright> getItemrights() {
		return this.itemrights;
	}

	public void setItemrights(List<Itemright> itemrights) {
		this.itemrights = itemrights;
	}

	public Itemright addItemright(Itemright itemright) {
		getItemrights().add(itemright);
		itemright.setMstEntity(this);

		return itemright;
	}

	public Itemright removeItemright(Itemright itemright) {
		getItemrights().remove(itemright);
		itemright.setMstEntity(null);

		return itemright;
	}

	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setMstEntity(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setMstEntity(null);

		return allrightsdetail;
	}


}