package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the itemrights database table.
 * 
 */
@Entity
@Table(name="itemrights")
@NamedQuery(name="Itemright.findAll", query="SELECT i FROM Itemright i")
public class Itemright implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="rightsfor")
	private int rightsFor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstEntity
	@ManyToOne
	@JoinColumn(name="entityID")
	private MstEntity mstEntity;

	//bi-directional many-to-one association to MstEntitytype
	@ManyToOne
	@JoinColumn(name="entitytypeID")
	private MstEntitytype mstEntitytype;

	//bi-directional many-to-one association to MstRole
	@ManyToOne
	@JoinColumn(name="roleID")
	private MstRole mstRole;

	//bi-directional many-to-one association to MstProfile
	@ManyToOne
	@JoinColumn(name="profileID")
	private MstProfile mstProfile;

	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="userID")
	private MstUserdetail mstUserdetail;

	//bi-directional many-to-one association to Itempermissionmapping
	@ManyToOne
	@JoinColumn(name="ItempermissionmappingID")
	private Itempermissionmapping itempermissionmapping;

	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="itemright")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	
	public Itemright() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public int getRightsFor() {
		return this.rightsFor;
	}

	public void setRightsFor(int rightsFor) {
		this.rightsFor = rightsFor;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstEntity getMstEntity() {
		return this.mstEntity;
	}

	public void setMstEntity(MstEntity mstEntity) {
		this.mstEntity = mstEntity;
	}

	public MstEntitytype getMstEntitytype() {
		return this.mstEntitytype;
	}

	public void setMstEntitytype(MstEntitytype mstEntitytype) {
		this.mstEntitytype = mstEntitytype;
	}

	public MstRole getMstRole() {
		return this.mstRole;
	}

	public void setMstRole(MstRole mstRole) {
		this.mstRole = mstRole;
	}

	public MstProfile getMstProfile() {
		return this.mstProfile;
	}

	public void setMstProfile(MstProfile mstProfile) {
		this.mstProfile = mstProfile;
	}

	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}

	public Itempermissionmapping getItempermissionmapping() {
		return this.itempermissionmapping;
	}

	public void setItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		this.itempermissionmapping = itempermissionmapping;
	}

	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setItemright(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setItemright(null);

		return allrightsdetail;
	}
}