package Tango3.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mst_usercontactdetail database table.
 * 
 */
@Entity
@Table(name="mst_usercontactdetail")
@NamedQuery(name="MstUsercontactdetail.findAll", query="SELECT m FROM MstUsercontactdetail m")
public class MstUsercontactdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="contactdetailID")
	private int contactDetailID;

	@Column(name="contactnumber")
	private String contactNumber;

	@Column(name="contactperson")
	private String contactPerson;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;
 
	private String relationship;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="userID")
	private MstUserdetail mstUserdetail;

	public MstUsercontactdetail() {
	}

	public int getContactDetailID() {
		return this.contactDetailID;
	}

	public void setContactDetailID(int contactDetailID) {
		this.contactDetailID = contactDetailID;
	}

	public String getContactNumber() {
		return this.contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}

}