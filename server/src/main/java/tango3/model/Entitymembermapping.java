package Tango3.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the entitymembermapping database table.
 * 
 */
@Entity
@Table(name="entitymembermapping")
@NamedQuery(name="Entitymembermapping.findAll", query="SELECT e FROM Entitymembermapping e")
public class Entitymembermapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date create_Ts;

	@Column(name="create_user_name")
	private String createUserName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modify_Ts;

	@Column(name="modify_user_name")
	private String modifyUserName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstEntity
	@ManyToOne
	@JoinColumn(name="entity_id")
	private MstEntity mstEntity;

	//bi-directional many-to-one association to MstMember
	@ManyToOne
	@JoinColumn(name="member_id")
	private MstMember mstMember;

	public Entitymembermapping() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreate_Ts() {
		return this.create_Ts;
	}

	public void setCreate_Ts(Date create_Ts) {
		this.create_Ts = create_Ts;
	}

	public String getCreateUserName() {
		return this.createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModify_Ts() {
		return this.modify_Ts;
	}

	public void setModify_Ts(Date modify_Ts) {
		this.modify_Ts = modify_Ts;
	}

	public String getModifyUserName() {
		return this.modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstEntity getMstEntity() {
		return this.mstEntity;
	}

	public void setMstEntity(MstEntity mstEntity) {
		this.mstEntity = mstEntity;
	}

	public MstMember getMstMember() {
		return this.mstMember;
	}

	public void setMstMember(MstMember mstMember) {
		this.mstMember = mstMember;
	}

}