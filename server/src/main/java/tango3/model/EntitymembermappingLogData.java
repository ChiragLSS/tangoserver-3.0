package Tango3.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * The persistent class for the entitymembermappingLogData database table.
 * 
 */
@Entity
@Table(name="entitymembermappinglogdata")
@NamedQuery(name="EntitymembermappingLogData.findAll", query="SELECT e FROM EntitymembermappingLogData e")
public class EntitymembermappingLogData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="create_ts")
	@JsonFormat(pattern = "MM/dd/yyyy")
	private Date createTs;

	@Column(name="create_user_name")
	private String createUserName;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	@JsonFormat(pattern = "MM/dd/yyyy")
	private Date endDate;

	@Temporal(TemporalType.DATE)
	@Column(name="modify_ts")
	@JsonFormat(pattern = "MM/dd/yyyy")
	private Date modifyTs;

	@Column(name="modify_user_name")
	private String modifyUserName;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	@JsonFormat(pattern = "MM/dd/yyyy")
	private Date startDate;

	//bi-directional many-to-one association to MstEntity
	@ManyToOne
	@JoinColumn(name="entity_id")
	private MstEntity mstEntity;

	//bi-directional many-to-one association to MstMember
	@ManyToOne
	@JoinColumn(name="member_id")
	private MstMember mstMember;

	
	
	public EntitymembermappingLogData() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateTs() {
		return this.createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getCreateUserName() {
		return this.createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getModifyTs() {
		return this.modifyTs;
	}

	public void setModifyTs(Date modifyTs) {
		this.modifyTs = modifyTs;
	}

	public String getModifyUserName() {
		return this.modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public MstEntity getMstEntity() {
		return this.mstEntity;
	}

	public void setMstEntity(MstEntity mstEntity) {
		this.mstEntity = mstEntity;
	}

	public MstMember getMstMember() {
		return this.mstMember;
	}

	public void setMstMember(MstMember mstMember) {
		this.mstMember = mstMember;
	}
	

}