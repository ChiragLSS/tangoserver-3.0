package Tango3.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the lss_user_token_management database table.
 * 
 */
@Entity
@Table(name="lss_user_token_management")
@NamedQuery(name="LssUserTokenManagement.findAll", query="SELECT l FROM LssUserTokenManagement l")
public class LssUserTokenManagement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="expired_at")
	private Date expiredAt;

	@Column(name="isactive")
	private byte isActive;

	private String token;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="userid")
	private MstUserdetail mstUserdetail;

	public LssUserTokenManagement() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpiredAt() {
		return this.expiredAt;
	}

	public void setExpiredAt(Date expiredAt) {
		this.expiredAt = expiredAt;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}

}