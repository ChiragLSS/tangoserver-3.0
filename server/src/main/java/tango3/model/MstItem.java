package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_item database table.
 * 
 */
@Entity
@Table(name="mst_item")
@NamedQuery(name="MstItem.findAll", query="SELECT m FROM MstItem m")
public class MstItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int itemID;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="displayname")
	private String displayName;

	@Column(name="itemname")
	private String itemName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstItem
	@ManyToOne
	@JoinColumn(name="parentID")
	private MstItem mstItem;

	//bi-directional many-to-one association to MstItem
	@OneToMany(mappedBy="mstItem")
	private List<MstItem> mstItems;

	//bi-directional many-to-one association to MstItemtype
	@ManyToOne
	@JoinColumn(name="itemtypeID")
	private MstItemtype mstItemtype;

	//bi-directional many-to-one association to Itempermissionmapping
	@OneToMany(mappedBy="mstItem")
	@JsonBackReference
	private List<Itempermissionmapping> itempermissionmappings;

	@ManyToOne
	@JoinColumn(name="componentID")
	private Tangocomponent tangocomponent;

	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstItem")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstItemParent")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetailsParent;
	
	public MstItem() {
	}

	public int getItemID() {
		return this.itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MstItem getMstItem() {
		return this.mstItem;
	}

	public void setMstItem(MstItem mstItem) {
		this.mstItem = mstItem;
	}

	public List<MstItem> getMstItems() {
		return this.mstItems;
	}

	public void setMstItems(List<MstItem> mstItems) {
		this.mstItems = mstItems;
	}

	public MstItem addMstItem(MstItem mstItem) {
		getMstItems().add(mstItem);
		mstItem.setMstItem(this);

		return mstItem;
	}

	public MstItem removeMstItem(MstItem mstItem) {
		getMstItems().remove(mstItem);
		mstItem.setMstItem(null);

		return mstItem;
	}

	public MstItemtype getMstItemtype() {
		return this.mstItemtype;
	}

	public void setMstItemtype(MstItemtype mstItemtype) {
		this.mstItemtype = mstItemtype;
	}

	public List<Itempermissionmapping> getItempermissionmappings() {
		return this.itempermissionmappings;
	}

	public void setItempermissionmappings(List<Itempermissionmapping> itempermissionmappings) {
		this.itempermissionmappings = itempermissionmappings;
	}

	public Itempermissionmapping addItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		getItempermissionmappings().add(itempermissionmapping);
		itempermissionmapping.setMstItem(this);

		return itempermissionmapping;
	}

	public Itempermissionmapping removeItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		getItempermissionmappings().remove(itempermissionmapping);
		itempermissionmapping.setMstItem(null);

		return itempermissionmapping;
	}

	public Tangocomponent getTangocomponent() {
		return this.tangocomponent;
	}

	public void setTangocomponent(Tangocomponent tangocomponent) {
		this.tangocomponent = tangocomponent;
	}
	
	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetails(Allrightsdetail allrightsdetails) {
		getAllrightsdetails().add(allrightsdetails);
		allrightsdetails.setMstItem(this);

		return allrightsdetails;
	}

	public Allrightsdetail removeAllrightsdetails(Allrightsdetail allrightsdetails) {
		getAllrightsdetails().remove(allrightsdetails);
		allrightsdetails.setMstItem(null);

		return allrightsdetails;
	}

	public List<Allrightsdetail> getAllrightsdetailsParent() {
		return this.allrightsdetailsParent;
	}

	public void setAllrightsdetailsParent(List<Allrightsdetail> allrightsdetailsParent) {
		this.allrightsdetailsParent = allrightsdetailsParent;
	}

	public Allrightsdetail addAllrightsdetailsParent(Allrightsdetail allrightsdetailsParent) {
		getAllrightsdetailsParent().add(allrightsdetailsParent);
		allrightsdetailsParent.setMstItemParent(this);

		return allrightsdetailsParent;
	}

	public Allrightsdetail removeAllrightsdetailsParent(Allrightsdetail allrightsdetailsParent) {
		getAllrightsdetailsParent().remove(allrightsdetailsParent);
		allrightsdetailsParent.setMstItemParent(null);

		return allrightsdetailsParent;
	}
	
}