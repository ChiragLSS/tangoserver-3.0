package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * The persistent class for the mst_userdetail database table.
 * 
 */
@Entity
@Table(name="mst_userdetail")
@NamedQuery(name="MstUserdetail.findAll", query="SELECT m FROM MstUserdetail m")
public class MstUserdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userID;

	@Column(name="access_card_id")
	private String accessCardId;

	@Column(name="access_card_no")
	private String accessCardNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="birth_date")
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	private Date birthDate;

	@Column(name="blood_group")
	private String bloodGroup;

	private String branch;

	@Column(name="change_password_key")
	private String changePasswordKey;

	@Column(name="changed_password")
	private byte changedPassword;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="contract_end_date")
	@JsonFormat(pattern="MM/dd/yyyy HH:mm:ss")
	private Date contractEndDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="MM/dd/yyyy")
	private Date create_Ts;

	@Column(name="create_user_name")
	private String createUserName;

	@Temporal(TemporalType.DATE)
	@Column(name="created_at")
	@JsonFormat(pattern="MM/dd/yyyy")
	private Date createdAt;

	private String degree;

	private String designation;

	private String email;

	@Column(name="employee_status")
	private String employeeStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="employeement_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date employeementDate;

	@Column(name="employement_type")
	private String employementType;

	@Column(name="employment_reason")
	private String employmentReason;

	@Column(name="employment_task_id")
	private String employmentTaskId;

	@Column(name="employment_task_key")
	private String employmentTaskKey;

	@Column(name="first_name")
	private String firstName;

	private String gender;

	@Column(name="graduation_year")
	private String graduationYear;

	@Column(name="has_jira_account")
	private byte hasJiraAccount;

	@Column(name="has_vpn_access")
	private byte hasVpnAccess;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="is_changed_password")
	private byte isChangedPassword;

	@Column(name="is_dropbox_configured")
	private byte isDropboxConfigured;

	@Column(name="is_email_configured_on_personal_device")
	private byte isEmailConfiguredOnPersonalDevice;

	@Column(name="is_facebook_access_from_home")
	private byte isFacebookAccessFromHome;

	@Column(name="is_facebook_configured_on_personal_device")
	private byte isFacebookConfiguredOnPersonalDevice;

	@Column(name="is_gm")
	private byte isGm;

	@Column(name="is_super_user")
	private byte isSuperUser;

	@Column(name="is_tango_admin")
	private byte isTangoAdmin;

	@Column(name="jira_user_master_task_id")
	private String jiraUserMasterTaskId;

	@Column(name="jira_user_master_task_key")
	private String jiraUserMasterTaskKey;

	@Column(name="join_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date joinDate;

	@Column(name="key_num")
	private String keyNum;

	@Column(name="last_college")
	private String lastCollege;

	@Column(name="last_name")
	private String lastName;
 
	@Temporal(TemporalType.DATE)
	@Column(name="leaving_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date leavingDate;

	@Column(name="locker_num")
	private String lockerNum;

	@Column(name="mobile_number_attached_to_fb")
	private String mobileNumberAttachedToFb;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="MM/dd/yyyy")
	private Date modify_Ts;

	@Column(name="modify_user_name")
	private String modifyUserName;

	private String password;

	@Column(name="personal_email")
	private String personalEmail;

	@Column(name="profile_image_path")
	private String profileImagePath;

	@Column(name="remember_token")
	private String rememberToken;

	@Column(name="service_provider_company_name")
	private String serviceProviderCompanyName;

	@Column(name="setup_jira_task_id")
	private String setupJiraTaskId;

	@Column(name="setup_jira_task_name")
	private String setupJiraTaskName;

	@Column(name="token_number_attached_to_fb")
	private String tokenNumberAttachedToFb;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	@JsonFormat(pattern="MM/dd/yyyy")
	private Date updatedAt;

	@Column(name="wiki_name")
	private String wikiName;

	//bi-directional many-to-one association to MstMember
	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<MstMember> mstMembers;

	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<LssAnalysetasklogview> lssAnalysetasklogviews;
	
	//bi-directional many-to-one association to Itemright
	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<Itemright> itemrights;

	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	//bi-directional many-to-one association to MstUsercontactdetail
	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<MstUsercontactdetail> mstUsercontactdetails;

	
	//bi-directional many-to-one association to LssUserTokenManagement
	@OneToMany(mappedBy="mstUserdetail")
	@JsonBackReference
	private List<LssUserTokenManagement> lssUserTokenManagements;
	
	public MstUserdetail() {
	}

	public int getUserID() {
		return this.userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getAccessCardId() {
		return this.accessCardId;
	}

	public void setAccessCardId(String accessCardId) {
		this.accessCardId = accessCardId;
	}

	public String getAccessCardNo() {
		return this.accessCardNo;
	}

	public void setAccessCardNo(String accessCardNo) {
		this.accessCardNo = accessCardNo;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBloodGroup() {
		return this.bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getChangePasswordKey() {
		return this.changePasswordKey;
	}

	public void setChangePasswordKey(String changePasswordKey) {
		this.changePasswordKey = changePasswordKey;
	}

	public byte getChangedPassword() {
		return this.changedPassword;
	}

	public void setChangedPassword(byte changedPassword) {
		this.changedPassword = changedPassword;
	}

	public Date getContractEndDate() {
		return this.contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public Date getCreate_Ts() {
		return this.create_Ts;
	}

	public void setCreate_Ts(Date create_Ts) {
		this.create_Ts = create_Ts;
	}

	public String getCreateUserName() {
		return this.createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getDegree() {
		return this.degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeStatus() {
		return this.employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

	public Date getEmployeementDate() {
		return this.employeementDate;
	}

	public void setEmployeementDate(Date employeementDate) {
		this.employeementDate = employeementDate;
	}

	public String getEmployementType() {
		return this.employementType;
	}

	public void setEmployementType(String employementType) {
		this.employementType = employementType;
	}

	public String getEmploymentReason() {
		return this.employmentReason;
	}

	public void setEmploymentReason(String employmentReason) {
		this.employmentReason = employmentReason;
	}

	public String getEmploymentTaskId() {
		return this.employmentTaskId;
	}

	public void setEmploymentTaskId(String employmentTaskId) {
		this.employmentTaskId = employmentTaskId;
	}

	public String getEmploymentTaskKey() {
		return this.employmentTaskKey;
	}

	public void setEmploymentTaskKey(String employmentTaskKey) {
		this.employmentTaskKey = employmentTaskKey;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGraduationYear() {
		return this.graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public byte getHasJiraAccount() {
		return this.hasJiraAccount;
	}

	public void setHasJiraAccount(byte hasJiraAccount) {
		this.hasJiraAccount = hasJiraAccount;
	}

	public byte getHasVpnAccess() {
		return this.hasVpnAccess;
	}

	public void setHasVpnAccess(byte hasVpnAccess) {
		this.hasVpnAccess = hasVpnAccess;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public byte getIsChangedPassword() {
		return this.isChangedPassword;
	}

	public void setIsChangedPassword(byte isChangedPassword) {
		this.isChangedPassword = isChangedPassword;
	}

	public byte getIsDropboxConfigured() {
		return this.isDropboxConfigured;
	}

	public void setIsDropboxConfigured(byte isDropboxConfigured) {
		this.isDropboxConfigured = isDropboxConfigured;
	}

	public byte getIsEmailConfiguredOnPersonalDevice() {
		return this.isEmailConfiguredOnPersonalDevice;
	}

	public void setIsEmailConfiguredOnPersonalDevice(byte isEmailConfiguredOnPersonalDevice) {
		this.isEmailConfiguredOnPersonalDevice = isEmailConfiguredOnPersonalDevice;
	}

	public byte getIsFacebookAccessFromHome() {
		return this.isFacebookAccessFromHome;
	}

	public void setIsFacebookAccessFromHome(byte isFacebookAccessFromHome) {
		this.isFacebookAccessFromHome = isFacebookAccessFromHome;
	}

	public byte getIsFacebookConfiguredOnPersonalDevice() {
		return this.isFacebookConfiguredOnPersonalDevice;
	}

	public void setIsFacebookConfiguredOnPersonalDevice(byte isFacebookConfiguredOnPersonalDevice) {
		this.isFacebookConfiguredOnPersonalDevice = isFacebookConfiguredOnPersonalDevice;
	}

	public byte getIsGm() {
		return this.isGm;
	}

	public void setIsGm(byte isGm) {
		this.isGm = isGm;
	}

	public byte getIsSuperUser() {
		return this.isSuperUser;
	}

	public void setIsSuperUser(byte isSuperUser) {
		this.isSuperUser = isSuperUser;
	}

	public byte getIsTangoAdmin() {
		return this.isTangoAdmin;
	}

	public void setIsTangoAdmin(byte isTangoAdmin) {
		this.isTangoAdmin = isTangoAdmin;
	}

	public String getJiraUserMasterTaskId() {
		return this.jiraUserMasterTaskId;
	}

	public void setJiraUserMasterTaskId(String jiraUserMasterTaskId) {
		this.jiraUserMasterTaskId = jiraUserMasterTaskId;
	}

	public String getJiraUserMasterTaskKey() {
		return this.jiraUserMasterTaskKey;
	}

	public void setJiraUserMasterTaskKey(String jiraUserMasterTaskKey) {
		this.jiraUserMasterTaskKey = jiraUserMasterTaskKey;
	}

	public Date getJoinDate() {
		return this.joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getKeyNum() {
		return this.keyNum;
	}

	public void setKeyNum(String keyNum) {
		this.keyNum = keyNum;
	}

	public String getLastCollege() {
		return this.lastCollege;
	}

	public void setLastCollege(String lastCollege) {
		this.lastCollege = lastCollege;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLeavingDate() {
		return this.leavingDate;
	}

	public void setLeavingDate(Date leavingDate) {
		this.leavingDate = leavingDate;
	}

	public String getLockerNum() {
		return this.lockerNum;
	}

	public void setLockerNum(String lockerNum) {
		this.lockerNum = lockerNum;
	}

	public String getMobileNumberAttachedToFb() {
		return this.mobileNumberAttachedToFb;
	}

	public void setMobileNumberAttachedToFb(String mobileNumberAttachedToFb) {
		this.mobileNumberAttachedToFb = mobileNumberAttachedToFb;
	}

	public Date getModify_Ts() {
		return this.modify_Ts;
	}

	public void setModify_Ts(Date modify_Ts) {
		this.modify_Ts = modify_Ts;
	}

	public String getModifyUserName() {
		return this.modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPersonalEmail() {
		return this.personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getProfileImagePath() {
		return this.profileImagePath;
	}

	public void setProfileImagePath(String profileImagePath) {
		this.profileImagePath = profileImagePath;
	}

	public String getRememberToken() {
		return this.rememberToken;
	}

	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}

	public String getServiceProviderCompanyName() {
		return this.serviceProviderCompanyName;
	}

	public void setServiceProviderCompanyName(String serviceProviderCompanyName) {
		this.serviceProviderCompanyName = serviceProviderCompanyName;
	}

	public String getSetupJiraTaskId() {
		return this.setupJiraTaskId;
	}

	public void setSetupJiraTaskId(String setupJiraTaskId) {
		this.setupJiraTaskId = setupJiraTaskId;
	}

	public String getSetupJiraTaskName() {
		return this.setupJiraTaskName;
	}

	public void setSetupJiraTaskName(String setupJiraTaskName) {
		this.setupJiraTaskName = setupJiraTaskName;
	}

	public String getTokenNumberAttachedToFb() {
		return this.tokenNumberAttachedToFb;
	}

	public void setTokenNumberAttachedToFb(String tokenNumberAttachedToFb) {
		this.tokenNumberAttachedToFb = tokenNumberAttachedToFb;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getWikiName() {
		return this.wikiName;
	}

	public void setWikiName(String wikiName) {
		this.wikiName = wikiName;
	}

	public List<MstMember> getMstMembers() {
		return this.mstMembers;
	}

	public void setMstMembers(List<MstMember> mstMembers) {
		this.mstMembers = mstMembers;
	}

	public MstMember addMstMember(MstMember mstMember) {
		getMstMembers().add(mstMember);
		mstMember.setMstUserdetail(this);

		return mstMember;
	}

	public MstMember removeMstMember(MstMember mstMember) {
		getMstMembers().remove(mstMember);
		mstMember.setMstUserdetail(null);

		return mstMember;
	}
	
	public List<LssAnalysetasklogview> getLssAnalysetasklogviews() {
		return this.lssAnalysetasklogviews;
	}

	public void setLssAnalysetasklogviews(List<LssAnalysetasklogview> lssAnalysetasklogviews) {
		this.lssAnalysetasklogviews = lssAnalysetasklogviews;
	}

	public LssAnalysetasklogview addLssAnalysetasklogview(LssAnalysetasklogview lssAnalysetasklogview) {
		getLssAnalysetasklogviews().add(lssAnalysetasklogview);
		lssAnalysetasklogview.setMstUserdetail(this);

		return lssAnalysetasklogview;
	}

	public LssAnalysetasklogview removeLssAnalysetasklogview(LssAnalysetasklogview lssAnalysetasklogview) {
		getLssAnalysetasklogviews().remove(lssAnalysetasklogview);
		lssAnalysetasklogview.setMstUserdetail(null);

		return lssAnalysetasklogview;
	}

	public List<Itemright> getItemrights() {
		return this.itemrights;
	}

	public void setItemrights(List<Itemright> itemrights) {
		this.itemrights = itemrights;
	}

	public Itemright addItemright(Itemright itemright) {
		getItemrights().add(itemright);
		itemright.setMstUserdetail(this);

		return itemright;
	}

	public Itemright removeItemright(Itemright itemright) {
		getItemrights().remove(itemright);
		itemright.setMstUserdetail(null);

		return itemright;
	}
	
	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setMstUserdetail(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setMstUserdetail(null);

		return allrightsdetail;
	}
	public List<MstUsercontactdetail> getMstUsercontactdetails() {
		return this.mstUsercontactdetails;
	}

	public void setMstUsercontactdetails(List<MstUsercontactdetail> mstUsercontactdetails) {
		this.mstUsercontactdetails = mstUsercontactdetails;
	}

	public MstUsercontactdetail addMstUsercontactdetail(MstUsercontactdetail mstUsercontactdetail) {
		getMstUsercontactdetails().add(mstUsercontactdetail);
		mstUsercontactdetail.setMstUserdetail(this);

		return mstUsercontactdetail;
	}

	public MstUsercontactdetail removeMstUsercontactdetail(MstUsercontactdetail mstUsercontactdetail) {
		getMstUsercontactdetails().remove(mstUsercontactdetail);
		mstUsercontactdetail.setMstUserdetail(null);

		return mstUsercontactdetail;
	}
	public List<LssUserTokenManagement> getLssUserTokenManagements() {
		return this.lssUserTokenManagements;
	}

	public void setLssUserTokenManagements(List<LssUserTokenManagement> lssUserTokenManagements) {
		this.lssUserTokenManagements = lssUserTokenManagements;
	}

	public LssUserTokenManagement addLssUserTokenManagement(LssUserTokenManagement lssUserTokenManagement) {
		getLssUserTokenManagements().add(lssUserTokenManagement);
		lssUserTokenManagement.setMstUserdetail(this);

		return lssUserTokenManagement;
	}

	public LssUserTokenManagement removeLssUserTokenManagement(LssUserTokenManagement lssUserTokenManagement) {
		getLssUserTokenManagements().remove(lssUserTokenManagement);
		lssUserTokenManagement.setMstUserdetail(null);

		return lssUserTokenManagement;
	}
	
}