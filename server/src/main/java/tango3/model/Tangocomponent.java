package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the tangocomponent database table.
 * 
 */
@Entity
@Table(name="tangocomponent")
@NamedQuery(name="Tangocomponent.findAll", query="SELECT t FROM Tangocomponent t")
public class Tangocomponent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int componentID;

	@Column(name="componentname")
	private String componentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	@Column(name="Isactive")
	private byte isActive;

	@Column(name="isonetimeconfiguration")
	private byte isOneTimeConfiguration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstItem
	@OneToMany(mappedBy="tangocomponent")
	@JsonBackReference
	private List<MstItem> mstItems;

	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="tangocomponent")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;

	
	public Tangocomponent() {
	}

	public int getComponentID() {
		return this.componentID;
	}

	public void setComponentID(int componentID) {
		this.componentID = componentID;
	}

	public String getComponentName() {
		return this.componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public byte getIsOneTimeConfiguration() {
		return this.isOneTimeConfiguration;
	}

	public void setIsOneTimeConfiguration(byte isOneTimeConfiguration) {
		this.isOneTimeConfiguration = isOneTimeConfiguration;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<MstItem> getMstItems() {
		return this.mstItems;
	}

	public void setMstItems(List<MstItem> mstItems) {
		this.mstItems = mstItems;
	}

	public MstItem addMstItem(MstItem mstItem) {
		getMstItems().add(mstItem);
		mstItem.setTangocomponent(this);

		return mstItem;
	}

	public MstItem removeMstItem(MstItem mstItem) {
		getMstItems().remove(mstItem);
		mstItem.setTangocomponent(null);

		return mstItem;
	}
	
	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setTangocomponent(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setTangocomponent(null);

		return allrightsdetail;
	}

}