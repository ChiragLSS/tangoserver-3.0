package Tango3.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_itemtype database table.
 * 
 */
@Entity
@Table(name="mst_itemtype")
@NamedQuery(name="MstItemtype.findAll", query="SELECT m FROM MstItemtype m")
public class MstItemtype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_at")
	private Date createdAt;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_at")
	private Date updatedAt;

	//bi-directional many-to-one association to MstItem
	@OneToMany(mappedBy="mstItemtype")
	@JsonBackReference
	private List<MstItem> mstItems;
	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstItemtype")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;


	public MstItemtype() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<MstItem> getMstItems() {
		return this.mstItems;
	}

	public void setMstItems(List<MstItem> mstItems) {
		this.mstItems = mstItems;
	}

	public MstItem addMstItem(MstItem mstItem) {
		getMstItems().add(mstItem);
		mstItem.setMstItemtype(this);

		return mstItem;
	}

	public MstItem removeMstItem(MstItem mstItem) {
		getMstItems().remove(mstItem);
		mstItem.setMstItemtype(null);

		return mstItem;
	}
	
	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setMstItemtype(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setMstItemtype(null);

		return allrightsdetail;
	}

}