package Tango3.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the mst_entitytype database table.
 * 
 */
@Entity
@Table(name="mst_entitytype")
@NamedQuery(name="MstEntitytype.findAll", query="SELECT m FROM MstEntitytype m")
public class MstEntitytype implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int level;

	private String name;

	//bi-directional many-to-one association to MstEntity
	@OneToMany(mappedBy="mstEntitytype")
	@JsonBackReference
	private List<MstEntity> mstEntities;

	//bi-directional many-to-one association to Itemright
	@OneToMany(mappedBy="mstEntitytype")
	private List<Itemright> itemrights;
	
	//bi-directional many-to-one association to Allrightsdetail
	@OneToMany(mappedBy="mstEntitytype")
	@JsonBackReference
	private List<Allrightsdetail> allrightsdetails;
	
	public MstEntitytype() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MstEntity> getMstEntities() {
		return this.mstEntities;
	}

	public void setMstEntities(List<MstEntity> mstEntities) {
		this.mstEntities = mstEntities;
	}

	public MstEntity addMstEntities1(MstEntity mstEntities) {
		getMstEntities().add(mstEntities);
		mstEntities.setMstEntityType(this);

		return mstEntities;
	}

	public MstEntity removeMstEntities(MstEntity mstEntities) {
		getMstEntities().remove(mstEntities);
		mstEntities.setMstEntityType(null);

		return mstEntities;
	}
	public List<Itemright> getItemrights() {
		return this.itemrights;
	}

	public void setItemrights(List<Itemright> itemrights) {
		this.itemrights = itemrights;
	}

	public Itemright addItemright(Itemright itemright) {
		getItemrights().add(itemright);
		itemright.setMstEntitytype(this);

		return itemright;
	}

	public Itemright removeItemright(Itemright itemright) {
		getItemrights().remove(itemright);
		itemright.setMstEntitytype(null);

		return itemright;
	}

	public List<Allrightsdetail> getAllrightsdetails() {
		return this.allrightsdetails;
	}

	public void setAllrightsdetails(List<Allrightsdetail> allrightsdetails) {
		this.allrightsdetails = allrightsdetails;
	}

	public Allrightsdetail addAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().add(allrightsdetail);
		allrightsdetail.setMstEntitytype(this);

		return allrightsdetail;
	}

	public Allrightsdetail removeAllrightsdetail(Allrightsdetail allrightsdetail) {
		getAllrightsdetails().remove(allrightsdetail);
		allrightsdetail.setMstEntitytype(null);

		return allrightsdetail;
	}

}