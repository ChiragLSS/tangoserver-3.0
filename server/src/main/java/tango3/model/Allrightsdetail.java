package Tango3.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the allrightsdetail database table.
 * 
 */
@Entity
@Table(name="allrightsdetail")
@NamedQuery(name="Allrightsdetail.findAll", query="SELECT a FROM Allrightsdetail a")
public class Allrightsdetail implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="displayname")
	private String displayName;

	@Column(name="ismaster")
	private byte isMaster;

	@Column(name="itemname")
	private String itemName;

	@Column(name="itemtypename")
	private String itemTypeName;

	@Column(name="jiratask")
	private int jiraTask;

	
	private String link;

	@Column(name="permissionname")
	private String permissionName;

	@Column(name="profilename")
	private String profileName;

	private byte self;

	private byte under;

	//bi-directional many-to-one association to Itempermissionmapping
	@ManyToOne
	@JoinColumn(name="itempermissionmappingID")
	private Itempermissionmapping itempermissionmapping;

	//bi-directional many-to-one association to Itemright
	@ManyToOne
	@JoinColumn(name="rightsID")
	private Itemright itemright;

	//bi-directional many-to-one association to MstEntity
	@ManyToOne
	@JoinColumn(name="entityID")
	private MstEntity mstEntity;

	//bi-directional many-to-one association to MstEntitytype
	@ManyToOne
	@JoinColumn(name="entitytypeID")
	private MstEntitytype mstEntitytype;

	//bi-directional many-to-one association to MstItem
	@ManyToOne
	@JoinColumn(name="itemID")
	private MstItem mstItem;

	//bi-directional many-to-one association to MstItem
	@ManyToOne
	@JoinColumn(name="parentID")
	private MstItem mstItemParent;

	//bi-directional many-to-one association to MstItemtype
	@ManyToOne
	@JoinColumn(name="itemtypeID")
	private MstItemtype mstItemtype;

	//bi-directional many-to-one association to MstProfile
	@ManyToOne
	@JoinColumn(name="profileID")
	private MstProfile mstProfile;

	//bi-directional many-to-one association to MstRole
	@ManyToOne
	@JoinColumn(name="roleID")
	private MstRole mstRole;

	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="userID")
	private MstUserdetail mstUserdetail;

	//bi-directional many-to-one association to Permission
	@ManyToOne
	@JoinColumn(name="permissionID")
	private Permission permission;

	//bi-directional many-to-one association to Tangocomponent
	@ManyToOne
	@JoinColumn(name="componentID")
	private Tangocomponent tangocomponent;

	public Allrightsdetail() {
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public byte getIsMaster() {
		return this.isMaster;
	}

	public void setIsMaster(byte isMaster) {
		this.isMaster = isMaster;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemTypeName() {
		return this.itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

	public int getJiraTask() {
		return this.jiraTask;
	}

	public void setJiraTask(int jiraTask) {
		this.jiraTask = jiraTask;
	}

	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPermissionName() {
		return this.permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getProfileName() {
		return this.profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public byte getSelf() {
		return this.self;
	}

	public void setSelf(byte self) {
		this.self = self;
	}

	public byte getUnder() {
		return this.under;
	}

	public void setUnder(byte under) {
		this.under = under;
	}

	public Itempermissionmapping getItempermissionmapping() {
		return this.itempermissionmapping;
	}

	public void setItempermissionmapping(Itempermissionmapping itempermissionmapping) {
		this.itempermissionmapping = itempermissionmapping;
	}

	public Itemright getItemright() {
		return this.itemright;
	}

	public void setItemright(Itemright itemright) {
		this.itemright = itemright;
	}

	public MstEntity getMstEntity() {
		return this.mstEntity;
	}

	public void setMstEntity(MstEntity mstEntity) {
		this.mstEntity = mstEntity;
	}

	public MstEntitytype getMstEntitytype() {
		return this.mstEntitytype;
	}

	public void setMstEntitytype(MstEntitytype mstEntitytype) {
		this.mstEntitytype = mstEntitytype;
	}

	public MstItem getMstItem() {
		return this.mstItem;
	}

	public void setMstItem(MstItem mstItem) {
		this.mstItem = mstItem;
	}

	public MstItem getMstItemParent() {
		return this.mstItemParent;
	}

	public void setMstItemParent(MstItem mstItemParent) {
		this.mstItemParent = mstItemParent;
	}

	public MstItemtype getMstItemtype() {
		return this.mstItemtype;
	}

	public void setMstItemtype(MstItemtype mstItemtype) {
		this.mstItemtype = mstItemtype;
	}

	public MstProfile getMstProfile() {
		return this.mstProfile;
	}

	public void setMstProfile(MstProfile mstProfile) {
		this.mstProfile = mstProfile;
	}

	public MstRole getMstRole() {
		return this.mstRole;
	}

	public void setMstRole(MstRole mstRole) {
		this.mstRole = mstRole;
	}

	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}

	public Permission getPermission() {
		return this.permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Tangocomponent getTangocomponent() {
		return this.tangocomponent;
	}

	public void setTangocomponent(Tangocomponent tangocomponent) {
		this.tangocomponent = tangocomponent;
	}

}