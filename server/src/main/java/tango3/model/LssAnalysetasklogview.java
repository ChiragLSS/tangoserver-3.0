package Tango3.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the lss_analysetasklogview database table.
 * 
 */
@Entity
@Table(name="lss_analysetasklogview")
@NamedQuery(name="LssAnalysetasklogview.findAll", query="SELECT l FROM LssAnalysetasklogview l")
public class LssAnalysetasklogview implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Task_ID")
	private String task_ID;

	@Column(name="Areas")
	private String areas;

	@Column(name="assigned_to")
	private String assignedTo;

	private String blocks;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Completed_date")
	private Date completed_date;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Created_Date")
	private Date created_Date;

	private String creator;

	@Column(name="depends_on")
	private String dependsOn;

	@Column(name="Diffs")
	private String diffs;

	@Column(name="file_name")
	private String fileName;

	private String groupname;

	@Column(name="insert_date")
	private Timestamp insertDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="Last_Updated")
	private Date last_Updated;

	@Column(name="Platforms")
	private String platforms;

	@Column(name="Priority")
	private String priority;

	private String projectname;

	@Column(name="Reason_For_Closing")
	private String reason_For_Closing;

	private int reviewedstatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_date")
	private Date startDate;

	@Column(name="Status")
	private String status;

	private String tags;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="target_date")
	private Date targetDate;

	@Column(name="Title")
	private String title;

	//bi-directional many-to-one association to MstUserdetail
	@ManyToOne
	@JoinColumn(name="user_id")
	private MstUserdetail mstUserdetail;

	@Column(name="entity_id")
	private int entityId;
	
	private int tmt_ID;

	public LssAnalysetasklogview() {
	}

	public String getTask_ID() {
		return this.task_ID;
	}

	public void setTask_ID(String task_ID) {
		this.task_ID = task_ID;
	}

	public String getAreas() {
		return this.areas;
	}

	public void setAreas(String areas) {
		this.areas = areas;
	}

	public String getAssignedTo() {
		return this.assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getBlocks() {
		return this.blocks;
	}

	public void setBlocks(String blocks) {
		this.blocks = blocks;
	}

	public Date getCompleted_date() {
		return this.completed_date;
	}

	public void setCompleted_date(Date completed_date) {
		this.completed_date = completed_date;
	}

	public Date getCreated_Date() {
		return this.created_Date;
	}

	public void setCreated_Date(Date created_Date) {
		this.created_Date = created_Date;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getDependsOn() {
		return this.dependsOn;
	}

	public void setDependsOn(String dependsOn) {
		this.dependsOn = dependsOn;
	}

	public String getDiffs() {
		return this.diffs;
	}

	public void setDiffs(String diffs) {
		this.diffs = diffs;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getGroupname() {
		return this.groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public Timestamp getInsertDate() {
		return this.insertDate;
	}

	public void setInsertDate(Timestamp insertDate) {
		this.insertDate = insertDate;
	}

	public Date getLast_Updated() {
		return this.last_Updated;
	}

	public void setLast_Updated(Date last_Updated) {
		this.last_Updated = last_Updated;
	}

	public String getPlatforms() {
		return this.platforms;
	}

	public void setPlatforms(String platforms) {
		this.platforms = platforms;
	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getProjectname() {
		return this.projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public String getReason_For_Closing() {
		return this.reason_For_Closing;
	}

	public void setReason_For_Closing(String reason_For_Closing) {
		this.reason_For_Closing = reason_For_Closing;
	}

	public int getReviewedstatus() {
		return this.reviewedstatus;
	}

	public void setReviewedstatus(int reviewedstatus) {
		this.reviewedstatus = reviewedstatus;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTags() {
		return this.tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getTargetDate() {
		return this.targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTmt_ID() {
		return this.tmt_ID;
	}

	public void setTmt_ID(int tmt_ID) {
		this.tmt_ID = tmt_ID;
	}
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}
	
	public MstUserdetail getMstUserdetail() {
		return this.mstUserdetail;
	}

	public void setMstUserdetail(MstUserdetail mstUserdetail) {
		this.mstUserdetail = mstUserdetail;
	}

}