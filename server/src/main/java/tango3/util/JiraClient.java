package Tango3.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.joda.time.DateTime;

import Tango3.util.UtilConstants;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

public class JiraClient {
	 private static final int JSON_INDENT = 4;
	private String username;
	private String password;
	private String jiraUrl;
	private JiraRestClient restClient;
	private Jira jiraobj;	
	public JiraClient(Jira jiraobj) {
		this.jiraobj=jiraobj;	   
	}
	
	public JiraClient() {
		 this.username = "lodestonebot";
		    this.password = "lodestonebot#123";
		    this.jiraUrl = "http://192.168.100.100:8080";
		    this.restClient = getJiraRestClient();
	}
	
	private JiraRestClient getJiraRestClient() {
	    return new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(getJiraUri(), this.username, this.password);
	}
	
	private URI getJiraUri() {
	    return URI.create(this.jiraUrl);
	}
	
	public String createIssue() {	 
	    IssueRestClient issueClient = jiraobj.getRestClient().getIssueClient();
	    IssueInputBuilder newIssue = new IssueInputBuilder();	    
	    newIssue.setProjectKey(jiraobj.getProjectKey());
	    newIssue.setIssueTypeId(jiraobj.getIssueTypeId());
	    newIssue.setSummary(jiraobj.getIssueSummary());
	    newIssue.setAssigneeName(jiraobj.getAssignee());
	    newIssue.setDescription(jiraobj.getIssueDescription());
	    // Subscribers has CustomField Name
	    newIssue.setFieldValue(UtilConstants.getJiraSubscriberFields, this.addSubcribers(jiraobj.getSubscriber()));	    
	    return issueClient.createIssue(newIssue.build()).claim().getKey();
	}
	
	public void updateIssue() {
		IssueInputBuilder newIssue = new IssueInputBuilder();	    
	    newIssue.setProjectKey(jiraobj.getProjectKey());
	    newIssue.setIssueTypeId(jiraobj.getIssueTypeId());
	    newIssue.setSummary(jiraobj.getIssueSummary());
	    newIssue.setAssigneeName(jiraobj.getAssignee());
	    newIssue.setDescription(jiraobj.getIssueDescription());
	    // Subscribers has CustomField Name
	    newIssue.setFieldValue(UtilConstants.getJiraSubscriberFields, this.addSubcribers(jiraobj.getSubscriber()));
	    jiraobj.getRestClient().getIssueClient()
	  	      .updateIssue(jiraobj.getIssueKey(),newIssue.build()) 
		      .claim();
	}
	private List<ComplexIssueInputFieldValue> addSubcribers(List<String> subscribersList) {		
		List<ComplexIssueInputFieldValue> fieldList = new ArrayList<ComplexIssueInputFieldValue>();
	    for (String aValue : subscribersList){
	      Map<String, Object> mapValues = new HashMap<String, Object>();
	      mapValues.put("name", aValue);
	      ComplexIssueInputFieldValue fieldValue = new ComplexIssueInputFieldValue(mapValues);
	      fieldList.add(fieldValue);	     
	    }
	    return fieldList;
	}
	
	public void updateIssueDescription() {
	    IssueInput input = new IssueInputBuilder()
	      .setDescription(jiraobj.getIssueDescription())
	      .build();
	    jiraobj.getRestClient().getIssueClient()
	      .updateIssue(jiraobj.getIssueKey(), input)
	      .claim();
	}
	
	public Issue getIssue() {
	    return jiraobj.getRestClient().getIssueClient()
	      .getIssue(jiraobj.getIssueKey()) 
	      .claim();
	}
	
	public void addComment() {		
		URI u=getIssue().getCommentsUri();
		jiraobj.getRestClient().getIssueClient().addComment(u, Comment.valueOf(jiraobj.getComment()));
	}
	
	public List<Comment> getAllComments() {
	    return StreamSupport.stream(getIssue().getComments().spliterator(), false)
	      .collect(Collectors.toList());
	}
	
	public void deleteIssue() {
		jiraobj.getRestClient().getIssueClient()
	      .deleteIssue(jiraobj.getIssueKey(), jiraobj.isDeleteSubTask())
	      .claim();
	}
	
	public void changeStatus() {
		Promise<Iterable<Transition>> s=jiraobj.getRestClient().getIssueClient().getTransitions(getIssue().getTransitionsUri());
		try {
			for(Transition t: s.get()) {
				if(t.getName().toUpperCase().equals(jiraobj.getStatus().toUpperCase())){
					jiraobj.getRestClient().getIssueClient().transition(getIssue(), new TransitionInput(t.getId()));
					break;
				}
			}
		} catch (InterruptedException | ExecutionException e) {			
			e.printStackTrace();
		}
	}
	
	public String getIssueSummary() {
		Issue issue=this.getIssue();
		return issue.getSummary();
	}
	
	public String getIssueStatus() {
		Issue issue=this.getIssue();
		if(issue.getStatus() != null)
			return issue.getStatus().getName();
		else
			return null;
	}
	
	public String getIssueResolution() {
		Issue issue=this.getIssue();
		if(issue.getResolution() != null)
			return issue.getResolution().getName();
		else
			return null;
	}
	
	public DateTime getIssueUpdationTime() {
		Issue issue=this.getIssue();
		return issue.getUpdateDate();
	}
}
