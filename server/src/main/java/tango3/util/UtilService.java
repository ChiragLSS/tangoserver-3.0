package Tango3.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UtilService {

	private String DATE_FORMAT = "dd-M-yyyy hh:mm:ss a z";
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

	@Autowired
	protected EntityManager em;
	
	public String createJiraTask() {
		Jira jiraObj=new Jira("lodestonebot","lodestonebot#123","http://192.168.100.100:8080","TES");		
		jiraObj.setIssueTypeId(3);
		jiraObj.setIssueSummary("Testing");
		jiraObj.setAssignee("ChetanPatel");
		jiraObj.setSubscriber(new ArrayList<String>(){{add("PravinTirthani");add("ChiragKhatsuriya");}});
		jiraObj.setIssueKey("TES-1362");
		jiraObj.setComment("Comment Test");
		jiraObj.setDeleteSubTask(true);
		jiraObj.setStatus("Cancel Request");
		JiraClient jc=new JiraClient(jiraObj);
		//Create Jira Task 
		String status=jc.createIssue();
		//Add Comment into the task key		
		jc.addComment();
		//Delete Issue
//		jc.deleteIssue();
		//Change the Status of Task
		jc.changeStatus();
		return status;
		
	}

//	public void authenticateUser(String customerId) {
//		 Authentication auth=SecurityContextHolder.getContext().getAuthentication();
//	 		if(auth!=null) {
//	 			String user =  auth.getName();
//	 			ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//	 			HttpServletRequest request1 = attrs.getRequest();		
//	 			TenantContextHolder.setTenantId(userService.getGuidByUserName(user));
//	 		}
//	 }
	
//	public String getCurrentUser() {
//		Authentication auth=SecurityContextHolder.getContext().getAuthentication();
//		String user = null;
//		if(auth!=null) {
//			user =  auth.getName();			
//		}
//		return user;
//	}
	
	// Convert Date Time to GMT Zone Date Time 
	public ZonedDateTime getGMTZoneDateTime(Date cutrrentDateTime, String timeZone) {
		// null check
		if (cutrrentDateTime == null) 
			return null;
		
		ZoneId fromTimeZone = ZoneId.systemDefault();
		ZoneId toTimeZone = ZoneId.of("GMT");  //Target timezone
        
		// default system timezone if passed null or empty
		if (!(timeZone == null || "".equalsIgnoreCase(timeZone.toString().trim()))) {
			fromTimeZone = ZoneId.of(timeZone);
		}
				
        //Zoned date time at source timezone
		Instant instant = Instant.ofEpochMilli(cutrrentDateTime.getTime());
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, fromTimeZone);

        ZonedDateTime currentTime = localDateTime.atZone(fromTimeZone);      
         
        //Zoned date time at target timezone
        ZonedDateTime currentGMTDateTime = currentTime.withZoneSameInstant(toTimeZone);
         
        return currentGMTDateTime;
	}
	
	// Convert Date Time to GMT Date Time 
	public Date getGMTDateTime(Date cutrrentDateTime, String timeZone) {
        //Zoned date time at target timezone
        ZonedDateTime currentGMTDateTime = getGMTZoneDateTime(cutrrentDateTime, timeZone);
         
        return Date.from(currentGMTDateTime.toInstant());
	}
    
	// Convert Date Time to GMT Date Time String
    public String getGMTDateTimeString(Date cutrrentDateTime, String timeZone) {
    	//Zoned date time at target timezone
        ZonedDateTime currentGMTDateTime = getGMTZoneDateTime(cutrrentDateTime, timeZone);
        
        return formatter.format(currentGMTDateTime);
    }
    
	// Convert GMT Date Time to Given timezone Zone Date Time
    public ZonedDateTime getLocalZoneDateTime(Date cutrrentDateTime, String timeZone) {
		// null check
		if (cutrrentDateTime == null) 
			return null;
		
		ZoneId fromTimeZone = ZoneId.of("GMT");  
		ZoneId toTimeZone = ZoneId.systemDefault(); //Target timezone
		
		// default system timezone if passed null or empty
		if (!(timeZone == null || "".equalsIgnoreCase(timeZone.toString().trim()))) {
			toTimeZone = ZoneId.of(timeZone);
		}
				
		//Zoned date time at source timezone
		Instant instant = Instant.ofEpochMilli(cutrrentDateTime.getTime());
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, fromTimeZone);

        ZonedDateTime currentTime = localDateTime.atZone(fromTimeZone);      
         
        //Zoned date time at target timezone
        ZonedDateTime currentLocalDateTime = currentTime.withZoneSameInstant(toTimeZone);
         
        return currentLocalDateTime;
	}
    
    // Convert GMT Date Time to Given timezone Date Time
    public Date getLocalDateTime(Date cutrrentDateTime, String timeZone) {
    	//Zoned date time at target timezone
        ZonedDateTime currentGMTDateTime = getLocalZoneDateTime(cutrrentDateTime, timeZone);
         
        return Date.from(currentGMTDateTime.toInstant());
    }
    
    // Convert GMT Date Time to Given timezone Date Time String
    public String getLocalDateTimeString(Date cutrrentDateTime, String timeZone) {
    	//Zoned date time at target timezone
        ZonedDateTime currentGMTDateTime = getLocalZoneDateTime(cutrrentDateTime, timeZone);
        
        return formatter.format(currentGMTDateTime);
    }
    
    // Get the Status of Customer JIRA Issue
    public String getJiraIssueStatus(String issueKey) {
		Jira jiraObj=new Jira("reddit.qa@lodestonesoftware.com","lsspl@121","https://reddit.atlassian.net/","R4A");
		//Jira jiraObj=new Jira("lodestonebot","lodestonebot#123","http://192.168.100.100:8080","TES");		
		jiraObj.setIssueKey(issueKey);
		JiraClient jc=new JiraClient(jiraObj);
		
		//Get Jira Status 
		String status=jc.getIssueStatus();
		
		return status;
	}
    
    // Sync JIRA Bug Status
    public void syncCustomerJiraIssueStatus() {
    	// Get Bug List
//    	List<ProductBugDetail> lstBugs = getProductBugList();
//    	
//    	if(lstBugs.size() > 0) {
//	    	// Customer JIRA Object
//	    	Jira jiraObj=new Jira("reddit.qa@lodestonesoftware.com","lsspl@121","https://reddit.atlassian.net/","R4A");
//	    	
//	    	// Fetch JIRA status of all the Bugs from the list
//	    	for (ProductBugDetail pbd : lstBugs) {
//	    		jiraObj.setIssueKey(pbd.getBugId());
//	    		JiraClient jc=new JiraClient(jiraObj);
//	    		pbd.setTitle(jc.getIssueSummary());
//	    		pbd.setIsBugFoundByAutomation(isBugFoundInAutomation(pbd.getTitle()));
//	    		pbd.setBugStatus(jc.getIssueStatus());
//	    		pbd.setResolution(jc.getIssueResolution());
//	    		pbd.setIssueUpdateTime(jc.getIssueUpdationTime().toDate());
//	    		pbd.setLastSyncTime(new Date());
//	    		
//	    		productBugDetailService.save(pbd);
//			}
//    	}
    }
    
    public static long daysBetweenDates(Date from, Date to) {
        return TimeUnit.DAYS.convert((to.getTime() - from.getTime()), TimeUnit.MILLISECONDS);
    }
  
    public static HashMap<String, Object> responseGenerator(String message,Object result,boolean status){
    	HashMap<String, Object> response = new HashMap<String,Object>(); 
    	if(status) {
    		response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("SUCCESS"));
			response.put(UtilConstants.getResponseFields.get("DATA"),result);
		}else {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("ERROR"));
			response.put(UtilConstants.getResponseFields.get("MSG"), message);
		}
    	return response;
    }
	
    
    public static List getTypesOFUser(){
    	List get = new ArrayList<>();
    	get.add("Employee");
    	get.add("Contract");
    	get.add("Service Providers");
    	return get;
    }
    
    
    public static List getTypeOfUserStatus(){
    	List get = new ArrayList<>();
    	get.add("Active");
    	get.add("Notice Period");
    	get.add("Pending Exit");
    	get.add("Past");
    	get.add("Unassigned");
    	return get;
    }
	
}
