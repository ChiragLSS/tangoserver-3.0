package Tango3.util;

import java.net.URI;
import java.util.List;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

public class Jira {	
	
	private String userName;
	private String password;	
	private String jiraUrl;
	private String projectKey;
	private long issueTypeId;
	private String issueSummary;
	private String issueDescription;
	private String assignee;
	private List<String> subscriber;
	private String newComment;
	private List<String> comments;
	private String issueKey;
	private boolean deleteSubTask;
	private String comment;
	private String status;
	
 	public Jira(String userName, String password, String jiraUrl, String projectKey) {
		this.userName=userName;
		this.password=password;
		this.jiraUrl=jiraUrl;
		this.projectKey=projectKey;		
	}
	public String getUserName() {
		return userName;
	}	
	public String getPassword() {
		return password;
	}		
	public JiraRestClient getRestClient() {
		 return new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(getJiraUri(), this.userName, this.password);
	}	
	public String getJiraUrl() {
		return jiraUrl;
	}	
	public String getProjectKey() {
		return projectKey;
	}	
	public long getIssueTypeId() {
		return issueTypeId;
	}
	public void setIssueTypeId(long issueTypeId) {
		this.issueTypeId = issueTypeId;
	}
	public String getIssueSummary() {
		return issueSummary;
	}
	public void setIssueSummary(String issueSummary) {
		this.issueSummary = issueSummary;
	}
	public String getIssueDescription() {
		return issueDescription;
	}
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public List<String> getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(List<String> arrayList) {
		this.subscriber = arrayList;
	}
	public String getNewComment() {
		return newComment;
	}
	public void setNewComment(String newComment) {
		this.newComment = newComment;
	}
	public List<String> getComments() {
		return comments;
	}
	public void setComments(List<String> comments) {
		this.comments = comments;
	}	
	private URI getJiraUri() {
	    return URI.create(this.jiraUrl);
	}	
	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}
	public String getIssueKey() {
		return this.issueKey;
	}
	public boolean isDeleteSubTask() {
		return deleteSubTask;
	}
	public void setDeleteSubTask(boolean deleteSubTask) {
		this.deleteSubTask = deleteSubTask;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
