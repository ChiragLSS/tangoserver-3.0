package Tango3.util;

import java.util.HashMap;

public class UtilConstants {

	public static HashMap<String, String> getEntityPrimaryKey = new HashMap<String, String>(){{	
			put("RunDetails", "getId");
			put("FailureInvestigation", "getId");
	}};		
	public static String saveMessage="Successfully Saved";
	public static String updateMessage="Updated Successfully";	
	public static String ErrorInSaveMessage="Error in Saving Record";	
	public static String deleteSuccessMessage="Deleted Successfully.";	
	public static String deleteFailMessage="Can not deleted record.";
	public static String getJiraSubscriberFields="customfield_10300";
	
	public static long defaultPageNumber=1;
	public static long defaultRecordPerPage=10;
	
	//Static Text Used in APP Starts 
	public static String getProductBugText_UPPER="PRODUCT BUG";
	public static String getProductChangeText_UPPER="PRODUCT CHANGE";
	public static String getAutomationBugText_UPPER="AUTOMATION BUG";
	public static String getPendingText_UPPER="PENDING";
	public static String getElementChangeText_UPPER="ELEMENT CHANGE";	
	public static String getUndeterministicText_UPPER="UNDETERMINISTIC";
	public static String getFailedText_UPPER="FAILED";
	
	public static String getDeveloperText="Developer";
	public static String getQaTeamText ="Qa Team";
	public static String getPendingText="Pending";
	public static String getSuggestedActionForProductBug="Fix failure/Update Bug";
	public static String getSuggestedActionForProductChange="Update Automation";
	public static String getSuggestedActionForAutomationBug="Update Automation";	
	public static String getSuggestedActionForElementChange="Update Automation";	
	public static String getSuggestedActionForUndeterministic="Update Automation";
	public static String getSuggestedActionForPending="Investigate and Update Automation";
	public static Object getFailedText="Failed";
	
	public static String getErrorStatus="Error";
	public static String getSuccessStatus="Success";
	public static String getSuccessMsgForPasswordChange="Successfully changed the password";
	public static String getErrorMsgForPasswordChange="Error in changing the password";
	public static String getNoDataFoundMsg="No Data Found";
	
	public static int UserMarksheetReturnDate = 18;
	
	//Static Text Used in APP Ends
	
	
	
	public static long countTotalpages(int totalCount,int recordPerPage) {
		return (recordPerPage == -1 ? 1 :  ((totalCount / recordPerPage) + (totalCount % recordPerPage >= 1 ? 1 : 0)));
	}
	
	public static int getPageNumber(long totalPages, int pageNumber) {
		return (int) (pageNumber>totalPages && totalPages > 0 ?totalPages:pageNumber);
	}

	public static HashMap<String, Integer> getEntityType = new HashMap<String,Integer>() {
		{
			put("Departnment",1);
			put("Project",2);
			put("Group",3);
			put("Team",4);
			put("Member",5);
		}
	};
	
	public static HashMap<String, Integer> getMemberRoleID = new HashMap<String,Integer>() {
		{
			put("TeamLeader",2);
			put("GroupLeader",4);
			put("DepartmentHead",1);
			put("Member",3);
			put("ProjectManager",5);
			put("POC",6);
		}
	};
	
	public static HashMap<String, String> getEntityName = new HashMap<String,String>() {
		{
			put("HRDepartment","HRDepartment");
			put("ITDepartment","ITDepartment");
		}
	};
	
	public static HashMap<String, String> getResponseStatus = new HashMap<String,String>() {
		{
	           	put("INFO" , "info");
				put("SUCCESS" , "success");
				put("ERROR" , "error");
	            put("DUPLICATE" , "duplicate");
	            put("OVERLAP" , "overlap");
	            put("WARNING" , "warning");
		}
	};
	
	public static HashMap<String, String> getResponseFields = new HashMap<String,String>() {
		{
			put("STATUS" , "status");
            put("MSG" , "msg");
            put("DATA" , "data");
            put("FLAG" , "flag");
		}
	};
	
	public static HashMap<String, String> getEmployeeStatus = new HashMap<String,String>() {
		{
			
            put("ACTIVE" , "Active");
            put("UNASSIGNED" , "Unassigned");
            put("PENDING_EXIT" , "Pending Exit");
            put("PAST_EMPLOYEE" , "Past Employee");
            put("UNASSIGNED" , "Unassigned");
		}
	};
	
	public static HashMap<String, String> getResponseMessage = new HashMap<String,String>() {
		{
			put("ERROR_IN_GETTING_SERVER_LIST" , "Error in getting  server list.");
            put("NO_DATA_FOUND" , "No data found for request");
            put("NO_DATA_FOUND_FOR_COMPONENT_CATEGORY" , "No data found for Component Category.");
            put("NO_DATA_FOUND_FOR_COMPONENT" , "No data found for Component.");
            put("NO_DATA_FOUND_FOR_COMPONENT_PERMISSION" , "No data found for Component Permission.");
            put("NO_DATA_FOUND_FOR_COMPONENT_JIRA_TASK" , "No data found for Component jira task.");
            put("COMPONENT_RIGHTS_SAVE_SUCCESFULLY" , "Component Rights save succesfully.");
            put("ERROR_IN_SAVING_COMPONENT_RIGHTS" , "Error in saving component rights.");
            put("NO_PERMISSION_RIGHTS_FOUND_FOR_COMPONENT" , "No rights found for this component.");
            put("ERROR_IN_GETTING_PERMISSION_RIGHTS_FOR_COMPONENT" , "Error in getting component permission rights.");
            put("NO_USER_LIST_FOR_TEAM" , "No userlist found for team.");
            put("NO_USER_LIST_FOR_TEAMS" , "No userlist found for teams.");
            put("NO_TEAM_LIST_FOR_DEPARTMENT" , "No team list found for department.");
            put("NO_TEAM_LEADER_LIST_FOR_DEPARTMENT" , "No team leader list found for group and department.");
            put("NO_TEAM_LIST_FOR_GROUP" , "No team list found for group.");
            put("NO_TEAM_LEADER_LIST_FOR_GROUP" , "No team leader list found for team.");
            put("NO_TEAM_LIST_FOR_USER" , "No team list found for user.");
            put("YOU_ARE_NOT_AUTHORIZE_TO_VIEW_DATA" , "You are not authorize to view data.");
            put("INCORRECT_RECORDID_PASSED" , "Inccorect Record id passed for Component request.");
            put("NO_DATA_FOR_RECORDID_PASSED" , "No data found for the request.");
            put("ERROR_IN_SAVING_REQUEST" , "Error in saving request.");
            put("REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Request for component Succesfully.");
            put("APPROVE_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Approve Request for component succesfully.");
            put("ERROR_IN_APPROVE_REQUEST" , "Error in approve request.");
            put("REJECT_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Reject Request for component succesfully.");
            put("ERROR_IN_REJECT_REQUEST" , "Error in reject request.");
            put("REMOVE_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Remove Request for component succesfully.");
            put("ERROR_IN_REMOVE_REQUEST" , "Error in remove request.");
            put("DELETE_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Delete Request for component succesfully.");
            put("ERROR_IN_DELETE_REQUEST" , "Error in delete request.");
            put("CANCEL_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "cancel Request for component succesfully.");
            put("ERROR_IN_CANCEL_REQUEST" , "Error in cancel request.");
            put("RETURN_DOCUMENT_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Request for Return Document succesfully.");
            put("ERROR_IN_RETURN_DOCUMENT_REQUEST" , "Error in return document request.");
            put("APPROVE_RETURN_DOCUMENT_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Approve return document Request for component succesfully.");
            put("ERROR_IN_APPROVE_RETURN_DOCUMENT_REQUEST" , "Error in approve return document request.");
            put("COLLECTBACK_RETURN_DOCUMENT_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Collect Back Document Request for component succesfully.");
            put("ERROR_IN_COLLECBACK_RETURN_DOCUMENT_REQUEST" , "Error in collect back return document request.");
            put("REJECT_RETURN_DOCUMENT_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Reject return document Request for component succesfully.");
            put("ERROR_IN_REJECT_RETURN_DOCUMENT_REQUEST" , "Error in reject return document request.");
            put("EXTENSION_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Extension Request for component succesfully.");
            put("ERROR_IN_EXTENSION_REQUEST" , "Error in extension request.");
            put("APPROVE_EXTENSION_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Approve extension Request for component succesfully.");
            put("ERROR_IN_APPROVE_EXTENSION_REQUEST" , "Error in approve extension request.");
            put("REJECT_EXTENSION_REQUEST_FOR_COMPONENT_SUCCESFULLY" , "Reject extension Request for component succesfully.");
            put("ERROR_IN_REJECT_EXTENSION_REQUEST" , "Error in reject extension request.");
            put("NO_DATA_FOUND_FOR_SAFE" , "No data found for safe.");
            put("NO_DATA_FOUND_FOR_DEPARTMENT_LIST" , "No data found for department list.");
            put("NO_DATA_FOUND_FOR_DEPARTMENT_HEAD_LIST" , "No data found for department head list.");
            put("NO_DATA_FOUND_FOR_GROUP_LIST" , "No data found for group list.");
            put("NO_DATA_FOUND_FOR_GROUP_LEADER_LIST" , "No data found for group leader list.");
            put("NO_DATA_FOUND_FOR_LOCKER_LIST" , "No data found for locker list.");
            put("NO_DATA_FOUND_FOR_UNASSIGNED_MOBILE_LIST" , "No data found for unassigned mobile list.");
            put("NO_DATA_FOUND_FOR_ASSIGNED_MOBILE_LIST" , "No data found for assigned mobile list.");
            put("NO_FILEDS_LABEL_FOUND_FOR_COMPONENT_REQUEST" , "No request fields and label found for component.");
            put("DELETE_SUCCESSFULLY" , "Delete Successfully.");
            put("UPDATE_SUCCESSFULLY" , "Update Successfully.");
            put("SAVE_SUCCESSFULLY" , "Save Successfully.");
            put("ADD_SUCCESSFULLY" , "Add Successfully.");
            put("FREEZE_SUCCESSFULLY" , "Freeze Successfully.");
            put("FAILED_TO_FETCH_TASK_REVIEW_DETAILS" , "No Data found for Task Review Details.");
            put("FAILED_TO_DELETE_TASK_REVIEW_DETAILS" , "Failed To Delete Task Review Details.");
            put("FAILED_TO_ADD_CORRECTIVE_ACTIONS" , "Failed To Add Corrective Actions.");
            put("FAILED_TO_UPDATE_TASK_REVIEW_DETAILS" , "Failed To Update Task Review Details.");
            put("FAILED_TO_SAVE_TASK_REVIEW_DETAILS" , "Failed To Save Task Review Details.");
            put("FAILED_TO_FETCH_MISSED_BUG_DETAILS" , "No Data found for Missed Bug Details.");
            put("FAILED_TO_SAVE_MISSED_BUG_DETAILS" , "Failed To Save Missed Bug Details.");
            put("FAILED_TO_UPDATE_MISSED_BUG_DETAILS" , "Failed To Update Missed Bug Details.");
            put("FAILED_TO_DELETE_MISSED_BUG_DETAILS" , "Failed To Delete Missed Bug Details.");
            put("TASK_ID_ALREADY_EXIST" , "TaskID Already Exist.");
            put("FAILED_TO_FREEAE_ASSESSMENT_RATING_PERIOD" , "Failed To Freeze Assessment Rating Period.");
            put("FAILED_TO_DELETE_ASSESSMENT_RATING_PERIOD" , "Failed To Delete Assessment Rating Period.");
            put("FAILED_TO_UPDATE_ASSESSMENT_RATING_PERIOD" , "Failed To Update Assessment Rating Period.");
            put("FAILED_TO_FETCH_TEAM_ASSESSMENT_RECORDS" , "No Data Found for Team Assessment Records.");
            put("FAILED_TO_FETCH_USER_ASSESSMENT_RECORDS" , "No Data Found for User Assessment Records.");
            put("FAILED_TO_SAVE_USER_ASSESSMENT_RATING" , "Failed To Save User Assessment Rating.");
            put("FAILED_TO_FREEAE_USER_ASSESSMENT_RATING" , "Failed To Freeze User Assessment Rating.");
            put("FAILED_TO_FETCH_USER_ASSESSMENT_RATING_HISTORY" , "No Data found for User Assessment Rating History.");
            put("FAILED_TO_FETCH_USER_ASSESSMENT_RATING_DATA" , "No Data found for User Assessment Rating Data.");
            put("FAILED_TO_FETCH_USERS_FOR_ASSESSMENT" , "No Data found for Users for Assessment.");
            put("FAILED_TO_SAVE_ASSESSMENT_PERIOD_FOR_SELF" , "Failed To Save Assessment Period for Self.");
            put("FAILED_TO_SAVE_ASSESSMENT_PERIOD_FOR_TEAM" , "Failed To Save Assessment Period for Team.");
            put("FAILED_TO_FETCH_TEAM_RATING_PERIOD" , "No Data Found for Team Rating Period.");
            put("NO_DATA_TO_SHOW" , "No Data To Show.");
            put("FAILED_TO_FETCH_CURRENT_TEAM_RATING_PERIOD_LIST" , "No Data Found for Current Team Rating Period List.");
            put("ENTERED_RATING_PERIOD_IS_DUPLICATE_WITH_EXISTING" , "Entered Rating Period Is Duplicate With Existing.");
            put("ENTERED_RATING_PERIOD_IS_OVERLAP_WITH_EXISTING" , "Entered Rating Period Is Overlap With Existing.");
            put("MONTH_WISE_CREATED_HIGH_MID_LOW_CHART_CREATION_FAILED" , "Month_Wise_Created_High_Mid_Low_Chart Creation Failed.");
            put("MONTH_WISE_CREATED_OPEN_CLOSE_CHART_CREATION_FAILED" , "Month_Wise_Created_Open_Close_Chart Creation Failed.");
            put("MONTH_WISE_HIGH_MID_LOW_CHART_CREATION_FAILED" , "Month_Wise_High_Mid_Low_Chart Creation Failed.");
            put("FAILED_TO_FETCH_TASK_ANALYSIS_PROFILE" , "No Data found for Task Analysis Profile.");
            put("FAILED_TO_CHECK_TEAM_PRODUCT_RECORD_EXIST" , "Failed To Check Team Product Record Exist.");
            put("FAILED_TO_ADD_TEAM_PRODUCT_RECORD" , "Failed To Add Team Product Record.");
            put("FAILED_TO_FETCH_TEAM_PRODUCT_RECORDS_LIST" , "No Data found for Team Product Records List.");
            put("FAILED_TO_UPDATE_TEAM_PRODUCT_TAG_RECORD" , "Failed To Update Team Product Tag Record.");
            put("FAILED_TO_FETCH_CONFIRM_DEVICE_OUTWARD" , "No Data found for Confirm Device Outward.");
            put("FAILED_TO_FETCH_OVER_DUE_DEVICES" , "No Data found for Over Due Devices.");
            put("FAILED_TO_FETCH_CURRENTLY_OUTWARD_DEVICES" , "No Data found for Currently Outward Devices.");
            put("FAILED_TO_FETCH_LATE_DEPOSITED_DEVICES" , "No Data found for Late Deposited Devices.");
            put("FAILED_TO_FETCH_FULL_VIEW_OF_OUTWARD_DEVICES" , "No Data found for Full View Of Outward Devices.");
            put("DEVICE_OUTWARDS_SUCCESSFULLY" , "Device Outwards Successfully.");
            put("FAILED_TO_DEVICE_OUTWARD" , "Failed To Device Outward.");
            put("FAILED_TO_UPDATE_EQUIPMENT_OUTWARD_REQUEST" , "Failed To Update Equipment Outward Request.");
            put("DEVICE_DEPOSITED_SUCCESSFULLY" , "Device Deposited Successfully.");
            put("FAILED_TO_DEVICE_DEPOSIT" , "Failed To Device Deposit.");
            put("USER_DATA_SAVE_SUCCESSFULLY" , "User Data Save Successfully.");
            put("ERROR_IN_SAVING_CONTACT_DETAILS" , "Error In Saving Contact Details.");
            put("ERROR_IN_SAVING_USER_DATA" , "Error In Saving User Data.");
            put("USERNAME_ALREADY_EXIST" , "Username Already Exist.");
            put("USER_ALREADY_EXIST" , "User Already Exist.");
            put("USERNAME_IS_AVAILABLE" , "Username Is Available.");
            put("NO_DEGREE_LIST_FOUND" , "No Degree List Found.");
            put("NO_BRANCH_LIST_FOUND" , "No Branch List Found.");
            put("NO_DATA_FOUND_FOR_USER_LIST" , "No Data found for User List.");
            put("NO_DATA_FOUND_FOR_PARTICULAR_REQUEST_ISTL" , "No Data found for Particular Request Is TL.");
            put("NO_DATA_FOUND_FOR_USER_TYPE" , "No Data found for User Type.");
            put("NO_DATA_FOUND_FOR_USER_FILTER" , "No Data found for User Filter.");
            put("NO_DATA_FOUND_FOR_TEAM_LIST" , "No Data found for Team List.");
            put("NO_DATA_FOUND_FOR_TL_FOR_USER" , "No Data found for TL For User.");
            put("NO_DATA_FOUND_FOR_USER_PROFILE_SUMMARY" , "No Data found for User Profile Summary.");
            put("NO_DATA_FOUND_FOR_TEAM_LEADERS" , "No Data found for Team Leader.");
            put("NO_DATA_FOUND_FOR_USER_PERSONAL_DETAILS" , "No Data found for User Personal Details.");
            put("NO_DATA_FOUND_FOR_USER_ASSIGNED_ACCESS" , "No Data found for User Assigned Access.");
            put("NO_DATA_FOUND_FOR_USER_INFORMATION" , "No Data found for User Information.");
            put("NO_DATA_FOUND_FOR_USER_TRANSITION_RECORD" , "No Data found for User Transition Record.");
            put("NO_DATA_FOUND_FOR_REQUESTED_USER_ID" , "No Data found for Requested User ID.");
            put("ERROR_IN_GETTING_DATA" , "Error In Getting Data.");
            put("LEAVING_DATE_FOR_USER_IS_SET" , "Leaving Date Of User Is Set.");
            put("ERROR_IN_SETTING_LEAVING_DATE" , "Error In Setting Leaving Date.");
            put("NO_DATA_FOUND_FOR_COMPONENT" , "No Data found for Component.");
            put("NO_DATA_FOUND_FOR_TOKEN" , "No Data found for Token.");
            put("NO_DATA_FOUND_FOR_PARENT_ENTITY" , "No data found for parent entity.");
            put("NO_DATA_FOUND_FOR_CHILD_ENTITY" , "No data found for child entity.");
            put("ENTITYADDED" , "Entity Added Successfully.");
            put("ERROR_IN_ADD_REQUEST" , "Error in adding request.");
            put("ERROR_IN_ENTRY_OF_ITEM_MAPPING" , "Error in entry of item mapping");
            put("DUPLICATE_NAME_OF_ITEM" , "Duplicate name of item");
            put("ERROR_IN_REMOVE_ITEM" , "Error in removing item");
            put("ERROR_IN_REMOVE_PERMISSION_MAPPING" , "Error in remove permission mapping");
            put("ERROR_IN_REMOVE_PARENT_MAPPING" , "Error in remove parent mapping");
            put("ERROR_IN_ENTRY_OF_PROFILE_NAME" , "Error in entry of profile name");
            put("ERROR_IN_ENTRY_OF_PROFILE_MAPPING" , "Error in entry of profile mapping");
            put("NO_DATA_FOUND_FOR_ENTITY_TYPE" , "No data found for entity type");
            put("NO_DATA_FOUND_FOR_ENTITY" , "No data found for entity");
            put("NO_DATA_FOUND_FOR_ROLE" , "No data found for role");
            put("RIGHTS_ADDED" , "Rights succesfully added");
            put("NO_DATA_FOUND_FOR_RIGHTS_ITEMS" , "No data found for rights items");
            put("NO_DATA_FOUND_FOR_RIGHTS_PROFILE" , "No data found for rights profile");
            put("NO_DATA_FOUND_FOR_RIGHTS_ITEMS_AND_PROFILE_ITEMS" , "No data found for rights of item and profile items");
            put("SAME_RIGHTS_ALREADY_ASSIGN" , "Rights are already assign");
            put("RIGHTS_UPDATED" , "Rights succesfully updated");
            put("ERROR_IN_UPDATING_RIGHTS" , "Error in updating rights");
            put("NO_COMPONENT_LIST_FOUND" , "No component List found");
            put("ERROR_IN_SELECTING_PERMISSiON" , "Error in selecting item permission");
            put("DUPLICATE_PROFILE_NAME" , "Duplicate Profile Name");
            put("ERROR_IN_SELECTING_PERMISSiON" , "Error in selecting item permission");
            put("FAILED_TO_FETCH_BUG_REPORTING_TOOL_LIST" , "No Data found for Bug Reporting Tools.");
            put("FAILED_TO_FETCH_REASON_FOR_TASK_LIST" , "No Data found for Reason For Task.");
            put("FAILED_TO_FETCH_BUG_REPORTER_LIST" , "No Data found for Bug Reporter.");
            put("NO_WORK_AREA_FOUND_FOR_REQUESTED_USER" , "No Work Area found for Requested User.");
            put("NO_WORK_AREA_FOUND_FOR_REQUESTED_TEMP_CARD" , "No Work Area found for Requested Temp Card.");
            put("NO_TEMP_CARD_FOUND_FOR_REQUESTED_USER" , "No Temp Card found for Requested User.");
            put("NO_DATA_FOUND_FOR_TEMP_ISSUE_CARD_STATUS_DETAILS" , "No Data found for Temporary Card Status Details.");
            put("NO_DATA_FOUND_FOR_ISSUED_TEMP_CARD_HISTORY" , "No Data found for Issued Temp Card History.");
            put("CARD_IS_ALREADY_ASSIGNED" , "Card Is Already Assigned.");
            put("FAILED_TO_ISSUE_TEMP_CARD" , "Failed To Issue Temp Card.");
            put("TEMP_CARD_ISSUED_SUCCESSFULLY" , "Temp Card Issued Successfully.");
            put("TEMP_CARD_DIPOSITED_SUCCESSFULLY" , "Temp Card Deposited Successfully.");
            put("FAILED_TO_DEPOSIT_TEMP_CARD" , "Failed To Deposit Temp Card.");
            put("NO_DATA_FOUND_FOR_EMPLOYEE_REPLACEMENT_CARD" , "No Data found for Employee Replacement Card.");
            put("NO_DATA_FOUND_FOR_VISITOR_CARD" , "No Data found for Visitor Card.");
            put("NO_VISITOR_FOUND" , "No Visitor Found.");
            put("NO_DATA_FOUND_FOR_VISITOR" , "No Data found for Visitor.");
            put("VISITOR_CARD_ISSUED_SUCCESSFULLY" , "Visitor Card Issued Successfully.");
            put("FAILED_TO_ISSUE_VISIOR_CARD" , "Failed To Issue Visitor Card.");
            put("FAILED_TO_UPDATE_VISITOR_CARD_ENTRY" , "Failed To Update Visitor Card Entry.");
            put("NO_WORK_AREA_FOUND_FOR_VISITOR_ACCESS" , "No Work Area found for Visitor Access.");
            put("FAILED_TO_CHECK_IS_RECORD_EXIST" , "Failed To Check Is Record Exist.");
            put("NO_DATA_FOUND_FOR_INTERVIEWING_SUMMARY_ON_GIVEN_FRAGMENT" , "No Data found for Interviewing Summary On Given Fragment.");
            put("FAILED_TO_DEPOSIT_VISITOR_CARD" , "Failed To Deposit Visitor Card.");
            put("VISITOR_CARD_DEPOSITED_SUCCESSFULLY" , "Visitor Card Deposited Successfully.");
            put("NO_DATA_FOUND_FOR_ASSESSMENT_RATING_AREAS" , "No Data found for Assessment Rating Areas.");
            put("NO_DATA_FOUND_FOR_ASSESSMENT_RATING_INSTRUCTIONS" , "No Data found for Assessment Rating Instructions.");
            put("NO_DATA_FOUND_FOR_QUALITY_CLASSIFICATION" , "No Data found for Quality Classification.");
            put("NO_DATA_FOUND_FOR_TYPE_OF_FAILURE" , "No Data found for Type Of Failure.");
            put("NO_DATA_FOUND_FOR_TEST_PLAN_UPDATE_STATUS" , "No Data found for Test Plan Update Status.");
            put("NO_DATA_FOUND_FOR_REASON_FOR_CLASSIFICATION" , "No Data found for Reason For Classification.");
            put("CHART_CREATION_FAILED" , "Chart Creation Failed.");
            put("NO_DATA_FOUND_FOR_TASK_ANALYSIS" , "No Data found for Task Analysis.");
            put("JIRA_TASK_ASSIGNEE_DATA_ADDED" , "JIRA task assignee data addedd");
            put("YOUR_ASSIGNEMNT_NOT_DONE_YET_CONTACT_ADMINSTRATOR" , "Your assignment not done yet please contact adminstrator.");
            put("ERROR_IN_DELETING_RIGHTS" , "Error in deleting rights");
            put("RIGHTS_SUCCESFULLY_DELETED" , "Rights deleted succesfully");
            put("NO_FILEDS_FOUND_FOR_COMPONENT_REQUEST" , "No fields found for component request");
            put("ERROR_IN_PROFILE_IMAGE" , "Error in profile image");
            put("REQUEST_ALREADY_EXIST_FOR_THIS_COMPONENT" , "Request already exist for this component");
            put("NO_DATA_FOUND_FOR_MISSED_BUG_REPORTER_DETAILS" , "No Data found for Missed Bug Reporter Details.");
            put("NO_LEAVE_EXIST_FOR_SELECTED_MONTH" , "No leave data for selected month");
            put("ERROR_IN_APPLYING_LEAVE" , "Error in applying leave");
            put("LEAVE_APPLIED_SUCCESSFULLY" , "Leave applied successfully");
            put("LEAVE_UPDATED_SUCCESSFULLY" , "Leave updated successfully");
            put("ERROR_IN_UPDATING_LEAVE" , "Error in updating leave");
            put("LEAVE_EXIST_FOR_SELECTED_DATE" , "Leave exist for selected date.");
            put("LEAVE_CANCELLED_SUCCESSFULLY" , "Leave cancelled successfully.");
            put("ERROR_IN_CANCELLING_LEAVE" , "Error in cancelling leave.");
            put("LEAVE_REJECTED_SUCCESSFULLY" , "Leave rejected successfully.");
            put("ERROR_IN_REJECTING_LEAVE" , "Error in rejecting leave.");
            put("LEAVE_APPROVED_SUCCESSFULLY" , "Leave approved successfully.");
            put("ERROR_IN_APPROVING_LEAVE" , "Error in approving leave.");
            put("CANCEL_REQUEST_SUCCESSFULLY_FOR_APPROVED_LEAVE" , "Cancel request successfully for approved leave.");
            put("ERROR_IN_CANCELLING_REQUEST_FOR_APPROVE_LEAVE" , "Error in cancelling request for approve leave.");
            put("APPROVED_FOR_CANCEL_APPROVE_LEAVE_SUCCESSFULLY","Approved for cancelling request for approve leave successfully.");
            put("REJECTED_FOR_CANCEL_APPROVE_LEAVE_SUCCESSFULLY","Rejected for cancelling request for approve leave successfully.");
            put("ERROR_IN_APPROVING_FOR_CANCEL_APPROVE_LEAVE","Error in approving for cancel approve leave.");
            put("ERROR_IN_REJECTING_FOR_CANCEL_APPROVE_LEAVE","Error in rejecting for cancel approve leave.");       
            put("REPORT_ATTENDANCE_USER_NOT_AUTHORIZED_FOR_UNIT","User accessed a unit that was not authorized in Tango Attendance system.");
            put("REPORT_ATTENDANCE_NOT_CONFIGURED","Attendace is not configured in Tango Attendance system.");
            put("EMPLOYEE_PUNCH_DETAILS_UPDATED_SUCCESSFULLY","Employee punch details updated sucessfully.");  
            put("NO_DATA_FOUND_FOR_EMPLOYEE_WISE_DEVICE_PUNCH","No data found for employee wise device punch report.");
            put("EMPLOYEE_LEAVE_ADJUSTMENT_SUCESSFULLY","Employee Leave Adjustment updated sucessfully.");
            put("REQUEST_ALREADY_EXIST_FOR_THIS_COMPONENT","Request already exist for this component");
            put("EMPLOYEEMENT_CHANGED_SUCCESSFULLY","Employeement Changed Successfully.");
            put("EMPLOYEEMENT_CHANGED_FAILURE","Failed to Change Employeement.");
            put("EMPLOYEE_LEAVE_ADJUSTMENT_SUCESSFULLY","Employee Leave Adjustment updated sucessfully.");
            put("NOT_GETTING_USER_DETAIL","Not Getting User Details.");
            put("NOT_GETTING_ENTITY_DETAIL","Not Getting Entity Details.");
            put("NOT_GETTING_TRANSITION_DETAIL","Not Getting Transition Details.");
		}
	};
	
	
	
}
