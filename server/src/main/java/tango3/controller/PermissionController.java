package Tango3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import Tango3.model.Permission;

@Controller
@RequestMapping(path="/permission")
public class PermissionController extends GenericController<Permission> {
	
	
}
