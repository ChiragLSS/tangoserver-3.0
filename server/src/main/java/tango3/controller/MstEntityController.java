package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.MstEntity;
import Tango3.service.MstEntityService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/mstEntity")
public class MstEntityController extends GenericController<MstEntity> {
	
	@Autowired
	private MstEntityService mstEntityService;
	
	
	@RequestMapping(path="/getByTypeID",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getByTypeID(@RequestParam int typeID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List<MstEntity> result =mstEntityService.getByTypeID(typeID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}	
	
	@RequestMapping(path="/getChildList",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getChildList(@RequestParam int entityID,@RequestParam int level){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = mstEntityService.getChildList(entityID,level);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getParentList",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getParentList(@RequestParam int entityID,@RequestParam int entityTypeID,@RequestParam int level){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = mstEntityService.getParentList(entityID,entityTypeID,level);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	 
	@RequestMapping(path="/getBelowLevelMember",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getBelowLevelMember(@RequestParam int userID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = mstEntityService.getBelowLevelMember(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/checkEntityExist",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> checkEntityExist(@RequestParam int entityID,@RequestParam int typeID,@RequestParam byte active){
		HashMap<String, Object> response = new HashMap<String,Object>();
		MstEntity result = mstEntityService.checkEntityExist(entityID,typeID,active);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getBelongsToList",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getBelongsToList(@RequestParam int userID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = mstEntityService.belongsTo(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getReportsToList",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getReportsToList(@RequestParam int userID,@RequestParam int typeID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = mstEntityService.reportsTo(userID,typeID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	
	
	@RequestMapping(path="/getMemberListFromEntityList",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getMemberListFromEntityList(@RequestParam int userID,@RequestParam int entityID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = mstEntityService.getMemberListFromEntityList(userID,entityID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NO_DATA_FOUND_FOR_CHILD_ENTITY"), result,result.size()!=0?true:false);
	}
	
}
