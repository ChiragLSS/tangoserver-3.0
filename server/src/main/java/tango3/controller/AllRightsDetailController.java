package Tango3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import Tango3.model.Allrightsdetail;
import Tango3.model.Permission;
import Tango3.service.AllRightsDetailService;

@RestController
@RequestMapping(path="/allrightsdetail")
public class AllRightsDetailController extends GenericController<Allrightsdetail> {

	
	@Autowired
	private AllRightsDetailService allRightsDetailService;

	
	@RequestMapping(path="/getAll")
	public List<Allrightsdetail> getAll(){
		return allRightsDetailService.getAll();
	}
}
