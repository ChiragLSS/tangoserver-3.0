package Tango3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import Tango3.model.MstEntitytype;
import Tango3.model.MstItemtype;
import Tango3.service.MstEntitytypeService;

@Controller
@RequestMapping(path="/mstItemtype")
public class MstItemtypeController extends GenericController<MstItemtype>  {
	
	
}
