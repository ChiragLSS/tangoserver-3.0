package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.Itemright;
import Tango3.service.ItemrightService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/Itemright")
public class ItemrightController extends GenericController<Itemright> {

	@Autowired
	private ItemrightService itemRightsService;
	
	@RequestMapping(path= "/getRightsOfItem",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getRightsOfItem(){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = itemRightsService.getRightofItem();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	
	@RequestMapping(path= "/getRightsOfProfile",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getRightsOfProfile(){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = itemRightsService.getRightsofProfile();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path= "/getRightsOfProfileItem",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getRightsOfProfileItem(){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = itemRightsService.getRightsofProfileItem();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
}
