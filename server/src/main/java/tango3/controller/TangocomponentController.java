package Tango3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import Tango3.model.Tangocomponent;

@Controller
@RequestMapping(path="/tangocomponent")
public class TangocomponentController extends GenericController<Tangocomponent> {
	
}
