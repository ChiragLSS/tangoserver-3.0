package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.LssUserTokenManagement;
import Tango3.service.LssUserTokenManagementService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/LssUserTokenManagement")
public class LssUserTokenManagementController extends GenericController<LssUserTokenManagement> {
	
	@Autowired
	private LssUserTokenManagementService lssUserTokenManagementService;
	
	@RequestMapping(path="/getUserDetailFromToken")
	public @ResponseBody HashMap<String, Object> getUserDetailFromToken(@RequestParam String token){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = lssUserTokenManagementService.getUserdetailFromToken(token);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
}
