package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.MstMember;
import Tango3.service.MstMemberService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/mstMember")
public class MstMemberController extends GenericController<MstMember>  {
	
	@Autowired
	private MstMemberService MstMemberService;
	
	@RequestMapping(path="/getListOfMemberIdFromUserID",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getListOfMemberIdFromUserID(@RequestParam int userID){
		HashMap<String, Object> response =  new HashMap<String,Object>();
		List result = MstMemberService.getListOfMemerIDFromUserID(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getUserDetailFromMemberID",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getUserDetailFromMemberID(
			@RequestParam int memberID,
			@RequestParam int userID
			){
		HashMap<String, Object> response =  new HashMap<String,Object>();
		List result = MstMemberService.getUserDetailFromMemberID(memberID, userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}

	@RequestMapping(path="/getUserIDFromMemberID",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getUserIDFromMemberID(
			@RequestParam int memberID
			){
		HashMap<String, Object> response =  new HashMap<String,Object>();
		HashMap<String, Integer> result = MstMemberService.getUserIDFromMemberID(memberID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/removeMember",method=RequestMethod.GET)
	public @ResponseBody int removeMember(
			@RequestParam int memberID
			){
		return MstMemberService.RemoveMember(memberID);
	}
	
	@RequestMapping(path="/getAllActiveMember",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> checkisMemberLevel(){
		HashMap<String, Object> response =  new HashMap<String,Object>();
		List result = MstMemberService.checkisMemberLevel();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getMemberDetail",method=RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getMemberDetail(@RequestParam int userID){
		HashMap<String, Object> response =  new HashMap<String,Object>();
		List result = MstMemberService.getMemberdetail(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
}
