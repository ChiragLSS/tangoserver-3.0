package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.EntitymembermappingLogData;
import Tango3.service.EntitymembermappingLogDataService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/EntitymembermappingLogData")
public class EntitymembermappingLogDataController extends GenericController<EntitymembermappingLogData> {
	
	@Autowired
	private EntitymembermappingLogDataService entitymembermappingLogDataService;
	
	@RequestMapping(path="/getTransitionRecord")
	public @ResponseBody HashMap<String, Object> getTransitionRecord(@RequestParam int userID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = entitymembermappingLogDataService.getTransitionRecord(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	
	
}