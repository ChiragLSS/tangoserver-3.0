package Tango3.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.Entitymapping;
import Tango3.service.EntitymappingService;

@Controller
@RequestMapping(path="/entityMapping")
public class EntitymappingController extends GenericController<Entitymapping> {
	
	@Autowired
	private EntitymappingService entityMappingService;
	
//	@RequestMapping(path="/getChild",method=RequestMethod.GET)
//	public @ResponseBody HashMap<String , Object> getChildList(@RequestParam int entityID,@RequestParam int parentEntityID){
//		return entityMappingService.getChild(entityID,parentEntityID);
//	}
	
}
