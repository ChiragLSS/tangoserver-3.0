package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.Entitymembermapping;
import Tango3.service.EntitymembermappingService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/entityMemberMapping")
public class EntitymembermappingController extends GenericController<Entitymembermapping> {
	
	@Autowired
	private EntitymembermappingService entitymembermappingService;
	
	@RequestMapping(path= "/getByEntityID",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getByEntityID(@RequestParam int entityID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = entitymembermappingService.getByEntityID(entityID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path= "/getLeaderByEntityID",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getLeaderByEntityID(@RequestParam int entityID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result = entitymembermappingService.getParentList(entityID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	
	@RequestMapping(path="/getMemberDeteilFromEntity",method=RequestMethod.GET,produces="application/json")
	public @ResponseBody HashMap<String, Object> getMemberDeteilFromEntity() {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = entitymembermappingService.getMemberDeteilFromEntity();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
		
	}
	
}