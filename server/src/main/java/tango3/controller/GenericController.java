package Tango3.controller;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.responsestatus.ResponseStatus;
import Tango3.service.GenericService;
import Tango3.util.UtilConstants;


public abstract class GenericController<T> {

	@Autowired
	private GenericService<T> genericService;	
	
	@GetMapping(path = "/", produces = { "application/json" })
	public @ResponseBody T findAll(@RequestHeader String customerId) {
//		TenantContextHolder.setTenantId(customerId);
		return genericService.findAll();		
	}
	
	@PostMapping(path = "/save", produces = { "application/json" })
	public @ResponseBody HashMap<String, String> save(@RequestBody T dataObject) {
		ResponseStatus response;
		long objectId=0;		
		try {
			genericService.save(dataObject);
		Method method = dataObject.getClass().getMethod(UtilConstants.getEntityPrimaryKey.get(dataObject.getClass().getSimpleName()));
		objectId=(long)method.invoke(dataObject);		
		if(objectId!=0) {
			response =new ResponseStatus("Success","",UtilConstants.saveMessage);
		}else {
			response =new ResponseStatus("Error","",UtilConstants.ErrorInSaveMessage);
		}
	}catch (Exception e) {
		System.out.println(e.getMessage());
		response =new ResponseStatus("Error","",UtilConstants.ErrorInSaveMessage);
	}
	return response.getStatus();
	}
	
	@PostMapping(path = "/update", produces = { "application/json" })
	public @ResponseBody HashMap<String, String> update(@RequestBody T dataObject,@RequestHeader String customerId) {		
//		TenantContextHolder.setTenantId(customerId);
		ResponseStatus response;
		long objectId=0;		
		try {
			genericService.save(dataObject);
		Method method = dataObject.getClass().getMethod(UtilConstants.getEntityPrimaryKey.get(dataObject.getClass().getSimpleName()));
		objectId=(long)method.invoke(dataObject);		
		if(objectId!=0) {
			response =new ResponseStatus("Success","",UtilConstants.updateMessage);
		}else {
			response =new ResponseStatus("Error","",UtilConstants.ErrorInSaveMessage);
		}
	}catch (Exception e) {
		System.out.println(e.getMessage());
		response =new ResponseStatus("Error","",UtilConstants.ErrorInSaveMessage);
	}
	return response.getStatus();
	}

	@PostMapping(path = "/delete", produces = { "application/json" })
	public @ResponseBody HashMap<String, String> delete(@RequestBody T dataObject) {
		ResponseStatus response;
		if (genericService.delete(dataObject)) {
			 response=new ResponseStatus("Success", UtilConstants.deleteSuccessMessage);
		} else {
			response=new ResponseStatus("Error", UtilConstants.deleteFailMessage);
		}
		return response.getStatus();
	}
	
}
