package Tango3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Tango3.model.LssAnalysetasklogview;
import Tango3.service.LssAnanlysisTaskLogService;

@RestController
@RequestMapping(path="/LssAnalysetasklogview")
public class LssAnanalysisTaskLogController extends GenericController<LssAnalysetasklogview> {
	
	@Autowired
	private LssAnanlysisTaskLogService lssAnanlysisTaskLogService;

	
	@RequestMapping(path="/getAll")
	public List<LssAnalysetasklogview> getAll(){
		return lssAnanlysisTaskLogService.getAll();
	}
	
}
