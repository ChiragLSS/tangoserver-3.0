package Tango3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import Tango3.model.MstRole;
import Tango3.service.MstRoleService;

@Controller
@RequestMapping(path="/mstRole")
public class MstRoleController extends GenericController<MstRole> {
	
	@Autowired
	private MstRoleService mstRoleService;
	
	
}
