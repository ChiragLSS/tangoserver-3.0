package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.MstUserdetail;
import Tango3.responsestatus.ResponseStatus;
import Tango3.service.MstMemberService;
import Tango3.service.MstUserdetailService;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;

@Controller
@RequestMapping(path="/mstUserDetail")
public class MstUserdetailController extends GenericController<MstUserdetail> {
	
	@Autowired
	private MstUserdetailService mstUserdetailService;
	@Autowired
	private MstMemberService mstMemberService;
	
	@RequestMapping(path="/getAllUserdetail")
	public @ResponseBody HashMap<String, Object> getAllUserDetail() {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.getAllUserDetail();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getByUserID")
	public @ResponseBody HashMap<String, Object> getAllUserDetail(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		MstUserdetail result = mstUserdetailService.findByID(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getDegreeListForNewUser")
	public @ResponseBody HashMap<String, Object> getDegreeListForNewUser() {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = mstUserdetailService.getDegreeListForNewUser();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getBranchListForNewUser")
	public @ResponseBody HashMap<String, Object> getBranchListForNewUser() {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result = mstUserdetailService.getBranchListForNewUser();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/checkUserNameIsExistsForNewUser")
	public @ResponseBody HashMap<String, Object> checkUserNameIsExistsForNewUser(@RequestParam String wikiName) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		boolean result = mstUserdetailService.checkUserNameIsExistsForNewUser(wikiName);
		if(result) {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("SUCCESS"));
			response.put(UtilConstants.getResponseFields.get("DATA"),UtilConstants.getResponseMessage.get("USERNAME_IS_AVAILABLE"));
		}else {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("ERROR"));
			response.put(UtilConstants.getResponseFields.get("MSG"), UtilConstants.getResponseMessage.get("USERNAME_ALREADY_EXIST"));
		}
		return response;
	}
	
	@RequestMapping(path="/getTypeOfUsers")
	public @ResponseBody List getTypeOfUsers() {
		return UtilService.getTypesOFUser();
	}
	
	@RequestMapping(path="/getTypeOfUsersStatus")
	public @ResponseBody List getTypeOfUsersStatus() {
		return UtilService.getTypeOfUserStatus();
	}
	
	
	@RequestMapping(path="/getActiveUsers")
	public @ResponseBody HashMap<String, Object> getActiveUsers() {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.ActiveUsers();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}

	@RequestMapping(path="/getUserProfileSummary")
	public @ResponseBody HashMap<String, Object> getUserProfileSummary(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.getUserProfileSummary(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	@RequestMapping(path="/getUserPersonalDetail")
	public @ResponseBody HashMap<String, Object> getUserPersonalDetail(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.getUserPersonalDetail(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	
	@RequestMapping(path="/getUserProfilePicWithBasicDetails")
	public @ResponseBody HashMap<String, Object> getUserProfilePicWithBasicDetails(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.getUserProfilePicWithBasicDetails(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.size()!=0?true:false);
	}
	@RequestMapping(path="/getUserNameFromUserID")
	public @ResponseBody HashMap<String, Object> getUserNameFromUserID(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		String result =  mstUserdetailService.getUserNameFromUserID(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result.length()!=0?true:false);
	}
	
	@RequestMapping(path="/isUserGM")
	public @ResponseBody HashMap<String, Object> isUserGM(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		byte result =  mstUserdetailService.isUserGm(userID);
		if(result != 0) {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("SUCCESS"));
			response.put(UtilConstants.getResponseFields.get("DATA"),UtilConstants.getResponseMessage.get("USERNAME_IS_AVAILABLE"));
		}else {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("ERROR"));
			response.put(UtilConstants.getResponseFields.get("MSG"), UtilConstants.getResponseMessage.get("USERNAME_ALREADY_EXIST"));
		}
		return response;
	}
	

	@RequestMapping(path="/isSuperUser")
	public @ResponseBody HashMap<String, Object> isSuperUser(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		byte result =  mstUserdetailService.isSuperUser(userID);
		if(result != 0) {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("SUCCESS"));
			response.put(UtilConstants.getResponseFields.get("DATA"),UtilConstants.getResponseMessage.get("USERNAME_IS_AVAILABLE"));
		}else {
			response.put(UtilConstants.getResponseFields.get("STATUS"), UtilConstants.getResponseStatus.get("ERROR"));
			response.put(UtilConstants.getResponseFields.get("MSG"), UtilConstants.getResponseMessage.get("USERNAME_ALREADY_EXIST"));
		}
		return response;
	}
	
	@RequestMapping(path="/getUserIdFromWikiName")
	public @ResponseBody HashMap<String, Object> getUserIdFromWikiName(@RequestParam String wikiName) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.getUserIdFromWikiName(wikiName);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}
	
	@RequestMapping(path="/updateLockerDetail")
	public @ResponseBody HashMap<String, Object> updateLockerDetail(@RequestParam int userID,@RequestParam String lockerNum,@RequestParam String keyNum) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.updateLockerDetail(userID, lockerNum, keyNum);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}
	
	@RequestMapping(path="/updateAccessCardNumber")
	public @ResponseBody HashMap<String, Object> updateAccessCardNumber(@RequestParam int userID,@RequestParam String AccessCardNumber) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.updateAccessCardNumber(userID, AccessCardNumber);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}
	
	@RequestMapping(path="/updateMobileAndTokenNumber")
	public @ResponseBody HashMap<String, Object> updateMobileAndTokenNumber(@RequestParam int userID,@RequestParam String TokenNumber,@RequestParam String MobileNumber) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.updateMobileAndTokenNumber(userID, TokenNumber, MobileNumber);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}
	
	@RequestMapping(path="/getLeavingDate")
	public @ResponseBody HashMap<String, Object> getLeavingDate(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		String result =  mstUserdetailService.getLeavingDate(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getEmployementType")
	public @ResponseBody HashMap<String, Object> getEmployementType(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		String result =  mstUserdetailService.getEmployementType(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/ExitUser")
	public @ResponseBody HashMap<String, Object> ExitUser(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.updateUserExit(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}

	@RequestMapping(path="/CheckUserExist")
	public @ResponseBody HashMap<String, Object> CheckUserExist(@RequestParam String firstName,@RequestParam String lastName) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		MstUserdetail result =  mstUserdetailService.checkUserExist(firstName, lastName);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	
	@RequestMapping(path="/setUserLeavingDate")
	public @ResponseBody HashMap<String, Object> setUserLeavingDate(@RequestParam int userID) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		int result =  mstUserdetailService.setUserLeavingDate(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=0?true:false);
	}

	@RequestMapping(path="/getUserDetailFromEmail")
	public @ResponseBody HashMap<String, Object> getUserDetailFromEmail(@RequestParam String email) {
		HashMap<String, Object> response = new HashMap<String,Object>();
		MstUserdetail result =  mstUserdetailService.getUserDetailFromEmail(email);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getUserInformation")
	public @ResponseBody HashMap<String, Object> getUserInformation(@RequestParam int userID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result =  mstUserdetailService.getUserInformation(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getNeccessaryUserDocumentList")
	public @ResponseBody HashMap<String, Object> getNeccessaryUserDocumentList(){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstUserdetailService.getNeccessaryUserDocumentList();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/getMarksheetCertificateList")
	public @ResponseBody HashMap<String, Object> getMarksheetCertificateList(){
		HashMap<String, Object> response = new HashMap<String,Object>();
		HashMap<String, Object> result =  mstUserdetailService.getMarksheetCertificateList();
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
	
	@RequestMapping(path="/isValidRequest")
	public @ResponseBody HashMap<String, Object> isValidRequest(@RequestParam int userID,@RequestParam int loginUserID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		boolean result =  mstUserdetailService.iValidRequest(userID, loginUserID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result);
	}
	
	@RequestMapping(path="/getListofAllUsers")
	public @ResponseBody HashMap<String, Object> getListofAllUsersByToken(@RequestParam int userID){
		HashMap<String, Object> response = new HashMap<String,Object>();
		List result =  mstMemberService.getMemberdetail(userID);
		return UtilService.responseGenerator(UtilConstants.getResponseMessage.get("NOT_GETTING_USER_DETAIL"), result,result!=null?true:false);
	}
}

