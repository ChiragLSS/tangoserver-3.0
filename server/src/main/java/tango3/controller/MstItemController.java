package Tango3.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.MstItem;
import Tango3.service.MstItemService;

@Controller
@RequestMapping(path="/mstItem")
public class MstItemController extends GenericController<MstItem>  {

	
	@Autowired
	private MstItemService mstItemService;
	
	@RequestMapping(path="/getItemParentMappingList")
	public @ResponseBody List getItemParentMappingList() {
		return mstItemService.getItemParentMappingList();
		
	}
	
	
	@RequestMapping(path="/getRightsDetail")
	public @ResponseBody List getRightsDetail(@RequestParam String itemParentID,
			@RequestParam String itemTypeID,
			@RequestParam String entityTypeID,
			@RequestParam String entityID,
			@RequestParam String parentEntityID,
			@RequestParam String roleID,
			@RequestParam int loginUserID,
			@RequestParam int userID
			) {
		return mstItemService.getRightsDetail(itemParentID,itemTypeID,entityTypeID,parentEntityID,entityID,roleID,loginUserID,userID);
		
	}
	
	@RequestMapping(path="/getDataForItemRightsOfUser")
	public @ResponseBody HashMap<String, Object> getDataForItemRightsOfUser(
			@RequestParam Integer userID,
			@RequestParam String token,
			@RequestParam List<String> itemTypeList,
			@RequestParam List<String> itemID,
			@RequestParam Integer itemParentID){
		return mstItemService.setDataForItemRightsOfUser(userID,token, itemID, itemTypeList, itemParentID);
	}
	
	@RequestMapping(path="/getSubItemRightsOfUserByItem")
	public @ResponseBody HashMap<String, Object> getSubItemRightsOfUserByItem(
			@RequestParam Integer userID,
			@RequestParam String token,
			@RequestParam List<String> itemTypeList,
			@RequestParam List<String> itemID,
			@RequestParam Integer itemParentID){
		return mstItemService.getSubItemRightsOfUserByItem(userID,token, itemID, itemTypeList, itemParentID);
	}
	
}
