package Tango3.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import Tango3.repository.GenericRepository;
import Tango3.util.UtilConstants;
import Tango3.util.UtilService;



public abstract class GenericService<T> implements Cloneable {
	
	@Autowired
	private GenericRepository<T> genericRepository;
	
	@Autowired
	public UtilService utilService;
	
	@Autowired	
	protected EntityManager em;
	
	
	@Transactional
	public T save(T dataObject) {			
		this.customValidation(dataObject);			
//		T objectBeforeSave = this.beforeSaveObjectForAudit(dataObject);
		genericRepository.save(dataObject);			
//		this.saveAudit(dataObject,objectBeforeSave,"");
		return (T) dataObject;
		
	}
	
	private boolean customValidation(T dataObject) {	
		return true;
	}
	
	@Transactional
	public List<T> save(List<T> dataObject) {		
		
			
		for(T obj:dataObject) {
			this.save(obj);
		}
		
		
		
		return  (List<T>) dataObject;
	}
	
	@Transactional
	public T saveCustomAudit(T dataObject){
		//SAVE INTO AUDIT TABLE
		try {			
			genericRepository.save(dataObject);
		} catch (Exception e) {
		    // generic exception handling
		    e.printStackTrace();
		}
		return null; 		
	}
	
	@Transactional
	public boolean delete(T dataObject) {	
		try{			
//			T objectBeforeSave = this.beforeSaveObjectForAudit(dataObject);
			
//			this.saveAudit(dataObject, objectBeforeSave, "DELETE");
			
			genericRepository.delete(dataObject);
		}
		catch(Exception ex){
			return false;
		}
		return true;
	}
	
	
	public boolean delete(List<T> dataObject) {
		for(T obj:dataObject) {
			this.delete(obj);
		}		
		return true;
	}
	
	public void saveAudit(T dataObject, T objectBeforeSave, String string) {
		
	}
	
	public T beforeSaveObjectForAudit(T dataObject) {
		return dataObject;
	}
	
	public T findAll() {
		return (T) genericRepository.findAll();
	}
	
	public List<Object> executeQueryForResultList(String query,long totalPages, int pageNumber,int recordPerPage,int totalCount) {		
		return this.em.createQuery(query)
				.setFirstResult(recordPerPage==-1?0:(UtilConstants.getPageNumber(totalPages, pageNumber-1)*recordPerPage))
				.setMaxResults(recordPerPage==-1?totalCount:recordPerPage).getResultList();
	}
	
	public List executeQuery(String query) {
		return this.em.createQuery(query) 
		.getResultList();	
	}
	
	public long executeCountQuery(String query) {		
		return (long)this.em.createQuery(query).getSingleResult();
	}
}	
