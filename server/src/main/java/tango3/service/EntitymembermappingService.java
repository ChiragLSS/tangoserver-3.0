package Tango3.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.Entitymembermapping;
import Tango3.model.EntitymembermappingLogData;
import Tango3.model.MstEntity;
import Tango3.model.MstMember;
import Tango3.repository.EntitymembermappingRepository;
import Tango3.util.UtilConstants;

@Service
public class EntitymembermappingService extends GenericService<Entitymembermapping> {
	
	@Autowired
	private EntitymembermappingRepository entitymembermappingRepository;
	@Autowired
	private MstEntityService mstEntityService;
	@Autowired
	private MstMemberService mstMemberService;
	@Autowired
	private EntitymembermappingLogDataService entitymembermappingLogDataService;
	
	
	public Entitymembermapping findByMemberID(int id) {
		return entitymembermappingRepository.findByMemberID(id);
	}
	
	
	public HashMap<String, Object> getByEntityID(int ID){
		HashMap<String, Object> result = new HashMap<String,Object>();
		result.put("Children", entitymembermappingRepository.getByEntityID(ID));
		return result;
	}
	
	public HashMap<String, Object> getParentList(int entityID){
		HashMap<String, Object> result = new HashMap<String,Object>();
		List<HashMap<String, String>> data = entitymembermappingRepository.getParentDataByEntityID(entityID);
		List<HashMap<String, String>> finalData = new ArrayList<HashMap<String,String>>();
		for(HashMap<String, String> getData : data) {
			if(!getData.get("EntityRoleName").equals("Member")) {
				finalData.add(getData);
			}
		}
		result.put("Parent", finalData);
		
		return result;
	}
	
	public HashMap<String, Object> getMemberByEntityID(int ID){
		HashMap<String, Object> result = new HashMap<String,Object>();
		List<HashMap<String, String>> data = entitymembermappingRepository.getParentDataByEntityID(ID);
		List<HashMap<String, String>> finalData = new ArrayList<HashMap<String,String>>();
		for(HashMap<String, String> getData : data) {
			if(getData.get("EntityRoleName").equals("Member")) {
				finalData.add(getData);
			}
		}
		if(finalData.size() != 0) {
			result.put(finalData.get(0).get("ParentEntityName"), finalData);
		}
		return result;
	}
	
	public Entitymembermapping checkMemberExist(int entityID,int typeID,int memberID,byte isActive) {
		return entitymembermappingRepository.checkMemberExist(entityID, typeID,memberID,isActive);
	}
	
	public int checkEntityExistIfNotCreate(int entityID,int memberID,int typeID) {
		if(typeID == UtilConstants.getEntityType.get("Member")) {
			Entitymembermapping existing = checkMemberExist(entityID, typeID, memberID, (byte)1);
			if(existing != null) {
				return existing.getId();
			}else {
				Entitymembermapping emm = entitymembermappingRepository.checkMemberExist(entityID, typeID, memberID, (byte) 0);
				if(emm == null) {
					MstMember member = mstMemberService.findById(memberID);
					MstEntity entity = mstEntityService.findByID(entityID);
					MstEntity saveObject = new MstEntity();
					emm = new Entitymembermapping();
					emm.setMstEntity(entity);
					emm.setMstMember(member);
					emm.setCreate_Ts(new Date());
					entitymembermappingRepository.save(emm);
					EntitymembermappingLogData logData = new EntitymembermappingLogData();
					logData.setMstEntity(entity);
					logData.setMstMember(member);
					logData.setCreateTs(new Date());
					logData.setStartDate(new Date());
					entitymembermappingLogDataService.save(logData);
					return emm.getId();
				}
			}
		}
		return 0;
	}
	
	public List getBelongToEntityIDsByUserID(int userID) {
		List result = entitymembermappingRepository.getBelongToEntityIDByUserID(userID);
		return result;
	}
	
	public List getReportsToEntityIDByUserID(int userID) {
		List result = entitymembermappingRepository.getReportsToEntityIDByUserID(userID);
		return result;
	}
	
	public List getMemberDeteilFromEntity() {
		return entitymembermappingRepository.getMemberDeteilFromEntity();
	}
	
	
}
