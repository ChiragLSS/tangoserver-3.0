package Tango3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.Tangocomponent;
import Tango3.repository.MstEntitytypeRepository;

@Service
public class TangocomponentService extends GenericService<Tangocomponent> {
	
	@Autowired
	private MstEntitytypeRepository mstEntitytypeRepository;

}
