package Tango3.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.Entitymembermapping;
import Tango3.model.MstMember;
import Tango3.model.MstRole;
import Tango3.model.MstUserdetail;
import Tango3.repository.MstMemberRepository;
import Tango3.util.UtilConstants;
import net.bytebuddy.build.HashCodeAndEqualsPlugin;

@Service
public class MstMemberService extends GenericService<MstMember> {
	
	@Autowired
	private MstMemberRepository mstMemberRepository;
	@Autowired
	private EntitymembermappingService entitymembermappingService;
	@Autowired
	private MstEntityService mstEntityService;
	@Autowired
	private MstUserdetailService mstUserdetailService;
	
	public MstMember findById(int id) {
		return mstMemberRepository.findById(id);
	}

	
	public List<HashMap<String, Integer>> getListOfMemerIDFromUserID(int userID){
		return mstMemberRepository.getListOfMemberIDFromUserID(userID);
	}
	
	public List getUserDetailFromMemberID(int memberID,int userID){
		return mstMemberRepository.getUserDetailFromMemberID(memberID, userID);
	}
	
	public HashMap<String, Integer> getUserIDFromMemberID(int memberID) {
		return mstMemberRepository.getUserIDFromMemberID(memberID);
	}
	
	
	public int RemoveMember(int memberID) {
		Entitymembermapping emm = entitymembermappingService.findByMemberID(memberID);
		entitymembermappingService.delete(emm);
		
		MstMember member = mstMemberRepository.findById(memberID);
		member.setIsActive((byte) 0);
		mstMemberRepository.save(member);
		
		return member.getId();
	}
	
	public HashMap<String, Object> getMemberListOfHRDepartment(){
		int hrDepartmentID = mstEntityService.getEntityIDByEntityName(UtilConstants.getEntityName.get("HRDepartment"));
		return mstEntityService.getBelowLevelMemberByEntityID(hrDepartmentID);
	}
	
	public HashMap<String, Object> getMemberListOfITDepartment(){
		int ITDepartmentID = mstEntityService.getEntityIDByEntityName(UtilConstants.getEntityName.get("ITDepartment"));
		return mstEntityService.getBelowLevelMemberByEntityID(ITDepartmentID);
	}
	
	public boolean checkIsHR(int userID){
		List<HashMap<String, Integer>> listOfMember = getListOfMemerIDFromUserID(userID);
		List HrDepartmentMemberList =  (List) getMemberListOfHRDepartment().get("Members");
		
		int isMember = 0;
		for(HashMap<String, Integer> get : listOfMember) {
			for(Object hrMember : HrDepartmentMemberList) {
				HashMap<String, String> getHrMemberData = (HashMap<String, String>) hrMember;
				if(get.get("memberID") == Integer.parseInt(String.valueOf(getHrMemberData.get("memberId")))) {
					isMember = 1;
				}
 			}  
		}
		return isMember == 1 ? true:false;
	}
	
	public boolean checkIsIT(int userID){
		List<HashMap<String, Integer>> listOfMember = getListOfMemerIDFromUserID(userID);
		List ITDepartmentMemberList = (List) getMemberListOfITDepartment().get("Members");
		
		int isMember = 0;
		for(HashMap<String, Integer> get : listOfMember) {
			for(Object ITMember : ITDepartmentMemberList) {
				HashMap<String, String> getITMemberData = (HashMap<String, String>) ITMember;
				if(get.get("memberID") == Integer.parseInt(String.valueOf(getITMemberData.get("memberId")))) {
					isMember = 1;
				}
 			}
		}
		return isMember == 1 ? true:false;
	}
	
	public boolean checkisMemberLevel(int memberID) {
		HashMap<String, String> result = mstMemberRepository.checkisMemberLevel(memberID);
		return result != null?true:false;
	}
	
	public List checkisMemberLevel() {
		List result = mstMemberRepository.getAllActiveMember();
		return result;
	}
	
	
	public List getMemberdetail(int userID){
		List allUserList = new ArrayList<>();
		List lowLevelMember = new ArrayList<>();
		List memberList = new ArrayList<>();
		List userdetailList = new ArrayList<>();
		if(checkIsHR(userID) || checkIsIT(userID)) {
			allUserList = mstUserdetailService.getAllUserDetail();
		}else {
			 HashMap<String,Object> data = mstEntityService.getBelowLevelMember(userID);
			 for(String keys : data.keySet()) {
				 List obj = (List) data.get(keys);
				 for(Object list : obj) {
					 HashMap<String, String> hashObj = (HashMap<String, String>) list;
					 userdetailList.add(getUserDetailFromMemberID(Integer.parseInt(String.valueOf(hashObj.get("memberId"))),-1));
				}
			 }
			 for(Object get : userdetailList) {
				 List userList = (List) get;
				 for(Object obj : userList) {
					allUserList.add(obj);
				 }
			 }
		}
		
		return allUserList;
		
	}
	
 }
