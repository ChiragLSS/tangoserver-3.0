package Tango3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.LssAnalysetasklogview;
import Tango3.model.MstUserdetail;
import Tango3.repository.LssAnanlysisTaskLogRepository;
import Tango3.repository.MstUserdetailRepository;

@Service
public class LssAnanlysisTaskLogService extends GenericService<LssAnalysetasklogview> {
	
	@Autowired
	private LssAnanlysisTaskLogRepository lssAnanlysisTaskLogRepository;
	
	
	public List<LssAnalysetasklogview> getAll(){
		return (List<LssAnalysetasklogview>) lssAnanlysisTaskLogRepository.findAll();
	}
}
