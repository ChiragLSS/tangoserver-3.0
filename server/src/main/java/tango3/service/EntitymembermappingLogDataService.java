package Tango3.service;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import Tango3.model.EntitymembermappingLogData;
import Tango3.repository.EntitymembermappingLogDataRepository;

@Service
public class EntitymembermappingLogDataService extends GenericService<EntitymembermappingLogData> {
	
	@Autowired
	private EntitymembermappingLogDataRepository entitymembermappingLogDataRepository;
	
	
	public EntitymembermappingLogData findById(int id) {
		return entitymembermappingLogDataRepository.findById(id);
	}
	
	@Transactional
	public EntitymembermappingLogData save(EntitymembermappingLogData saveObject) {
		saveObject.setCreateTs(new Date());
		saveObject.setStartDate(new Date());
		entitymembermappingLogDataRepository.save(saveObject);
		return  saveObject;
	}
	
	@Transactional
	public  EntitymembermappingLogData remove(EntitymembermappingLogData removeObject) {
		removeObject.setEndDate(new Date());
		removeObject.setModifyTs(new Date());
		entitymembermappingLogDataRepository.save(removeObject);
		return removeObject;
	}
	
	public List getTransitionRecord(int userID){
		return entitymembermappingLogDataRepository.getTransitionRecord(userID);
	}
	
	
}
