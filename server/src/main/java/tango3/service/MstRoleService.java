package Tango3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.MstRole;
import Tango3.repository.MstRoleRepository;

@Service
public class MstRoleService extends GenericService<MstRole> {
	
	@Autowired
	private MstRoleRepository mstRoleRepository;

}
