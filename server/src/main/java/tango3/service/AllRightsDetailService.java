package Tango3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.Allrightsdetail;
import Tango3.repository.AllRightsDetailRepository;

@Service
public class AllRightsDetailService extends GenericService<Allrightsdetail> {

	
	@Autowired
	private AllRightsDetailRepository allRightsDetailRepository;
	
	public List<Allrightsdetail> getAll() {
		return (List<Allrightsdetail>) allRightsDetailRepository.findAll();
	}
	
}

