package Tango3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.MstEntity;
import Tango3.model.MstEntitytype;
import Tango3.repository.MstEntitytypeRepository;

@Service
public class MstEntitytypeService extends GenericService<MstEntitytype> {
	
	@Autowired
	private MstEntitytypeRepository mstEntitytypeRepository;

}
