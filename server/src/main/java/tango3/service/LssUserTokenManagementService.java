package Tango3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.LssUserTokenManagement;
import Tango3.repository.LssUserTokenManagementRepository;

@Service
public class LssUserTokenManagementService extends GenericService<LssUserTokenManagement> {
	
	
	@Autowired
	private LssUserTokenManagementRepository lssUserTokenManagementRepository;
	
	public List getUserdetailFromToken(String token){
		return lssUserTokenManagementRepository.getUserDetailFromToken(token);
	}
	
	
}
