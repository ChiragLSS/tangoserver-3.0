package Tango3.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.aspectj.asm.IElementHandleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import Tango3.model.MstItem;
import Tango3.repository.MstItemRepository;

@Service
public class MstItemService extends GenericService<MstItem> {

	@Autowired
	private MstItemRepository mstItemRepository;
	
	@Autowired
	private LssUserTokenManagementService lssUserTokenManagementService;
	
	public List getItemParentMappingList() {
		return mstItemRepository.getItemParentMappingList();
	}
	
	
	
	
	public List getRightsDetail(String itemParentID,String itemtypeID,String entityTypeID,String parentEntityID,String entityID,String roleID,int loginUserID,int userID) {
		
		String CommonQuery = "select new map(mi.itemID as itemID,parent.tangocomponent.componentID as componentID,parent.mstItemtype.id as itemTypeID,mi.itemName as itemName,parent.displayName as displayName,mi.mstItem.itemID as parentID,parent.permission.permissionID as permissionID,parent.jiraTask as jiraTask,parent.link as link,parent.self as self,parent.under as under) from MstItem mi LEFT JOIN mi.allrightsdetails parent WHERE 1=1 ";
		
		if(!itemtypeID.equals(null) && !itemtypeID.equals("") ) {
			CommonQuery += " AND mi.mstItemtype.id IN ('"+itemtypeID+"') ";
		}
		
		String QueryWhere1 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere1 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") AND parent.mstEntity.id IS NULL AND parent.mstRole.roleID IS NULL ";
		}
		String QueryWhere2 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere2 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") ";
		}
		if(!parentEntityID.equals(null) && !parentEntityID.equals("")) {
			QueryWhere2 += " AND parent.mstEntity.id IN ("+parentEntityID+") AND parent.mstRole.roleID IS NULL ";
		}
		
		String QueryWhere3 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere3 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") ";
		}
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere3 += " AND parent.mstEntity.id IS NULL AND parent.mstRole.roleID IN ("+roleID+") ";
		}

		String QueryWhere4 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere4 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") ";
		}
		if(!parentEntityID.equals(null) && !parentEntityID.equals("")) {
			QueryWhere4 += " AND parent.mstEntity.id IN ("+parentEntityID+") ";
		}
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere4 += " AND parent.mstRole.roleID IN ("+roleID+") ";
		}
		
		String QueryWhere5 = "";
		if(!entityID.equals(null) && !entityID.equals("")) {
			QueryWhere5 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IN ("+entityID+") AND parent.mstRole.roleID IS NULL ";
		}
		
		String QueryWhere6 = "";
		if(!parentEntityID.equals(null) && !parentEntityID.equals("")) {
			QueryWhere6 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IN ("+parentEntityID+") ";
		}
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere6 += " AND parent.mstRole.roleID IN ("+roleID+") ";
		}
		
		String QueryWhere7 = "";
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere7 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstRole.roleID IN ("+roleID+") ";
		}
		
		String QueryWhere8 = "";
		if(!entityID.equals(null) && !entityID.equals("")) {
			QueryWhere8 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IN ("+entityID+") ";
		}
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere8 += " AND parent.mstRole.roleID IN ("+roleID+") ";
		}
		
		
		String QueryWhere9 = "";
		if(!entityID.equals(null) && !entityID.equals("")) {
			QueryWhere9 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IN ("+entityID+") AND parent.mstRole.roleID IS NULL ";
		}
		
		
		String QueryWhere10 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere10 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") ";
		}
		if(!entityID.equals(null) && !entityID.equals("")) {
			QueryWhere10 += " AND parent.mstEntity.id IN ("+entityID+") AND parent.mstRole.roleID IS NULL ";
		}
		
		String QueryWhere11 = "";
		if(!entityTypeID.equals(null) && !entityTypeID.equals("")) {
			QueryWhere11 += " AND parent.mstEntitytype.id IN ("+entityTypeID+") ";
		}
		if(!entityID.equals(null) && !entityID.equals("")) {
			QueryWhere11 += " AND parent.mstEntity.id IN ("+entityID+") ";
		}
		if(!roleID.equals(null) && !roleID.equals("")) {
			QueryWhere11 += " AND parent.mstRole.roleID IN ("+roleID+") ";
		}
		
		String QueryWhere12 = "";
		if(userID != -1) {
			QueryWhere12 += " AND parent.mstEntitytype.id IS NULL AND parent.mstEntity.id IS NULL  AND parent.mstUserdetail.userID = "+userID+" ";
		}
		
		
		 List<HashMap<String, String>> listOfItem1 = this.executeQuery(CommonQuery + QueryWhere1);
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere2));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere3));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere4));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere5));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere6));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere7));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere8));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere9));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere10));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere11));
		 listOfItem1.addAll(this.executeQuery(CommonQuery + QueryWhere12));
		
		
		List<Integer> listOfKey = new ArrayList<Integer>();
		listOfKey.add(Integer.parseInt(itemParentID));

		List<HashMap<String, String>> itemLists = listOfItem1.stream().distinct().collect(Collectors.toList()); 
		
		List result = new ArrayList<>();
		if(itemParentID != null) {
			for(HashMap<String, String> get : itemLists) {
				Integer obj = Integer.parseInt(String.valueOf(get.get("parentID")));
				Integer itemID = Integer.parseInt(String.valueOf(get.get("itemID")));
				byte self = Byte.parseByte(String.valueOf(get.get("self")));
				byte under = Byte.parseByte(String.valueOf(get.get("under")));
				int i = 0;
				for(Integer key : listOfKey) {
					if(loginUserID == userID) {
						if(obj == key && self == 1) {
							result.add(get);
							i = 1;
						}
					}else if(loginUserID != userID) {
						if(obj == key &&  under == 1 ) {
							result.add(get);
							i = 1;
						}
					}
				}
				if(i == 1) {
					listOfKey.add(itemID);
					listOfKey = listOfKey.stream().distinct().collect(Collectors.toList());
				}
			}
		}else {
			result.addAll(listOfItem1);
		}
		return result;
	}
	
	
	public HashMap<String, Object> setDataForItemRightsOfUser(Integer userID,String token,List<String> itemID,List<String> itemTypeList,int itemParentID) {
		HashMap<String, Object> result = new HashMap<String,Object>();
		
		if(itemID.size() != 0) {
			result.put("itemID", itemID.stream().collect(Collectors.joining(",")));
		}
		
			result.put("itemTypeList",itemTypeList.size()!=0?itemTypeList.stream().collect(Collectors.joining(",")):null);
		
		
		result.put("itemParentID",itemParentID!=-1?itemParentID:0);
		List userDetail = lssUserTokenManagementService.getUserdetailFromToken(token);
		if(userDetail.size() != 0) {
			Object getFirst = userDetail.get(0);
			HashMap<String, String> getFirstData = (HashMap<String, String>) getFirst;
			result.put("loginUserID", getFirstData.get("userID"));
			List<String> entityIDs = new ArrayList<>();
			List<String> entityTypeIDs = new ArrayList<>();
			List<String> parentIDs = new ArrayList<>();
			List<String> roleIDs = new ArrayList<>();
			for(Object get : userDetail) {
				HashMap<String, String> getData = (HashMap<String, String>)get;
				if(!String.valueOf(getData.get("entityId")).equals(null) && !String.valueOf(getData.get("roleID")).equals(null) && !String.valueOf(getData.get("parentID")).equals(null) && !String.valueOf(getData.get("typeID")).equals(null)) {
					if(entityIDs.size() != 0) {
						if(!entityIDs.contains(String.valueOf(getData.get("entityId")))) {
							entityIDs.add(String.valueOf(getData.get("entityId")));
						}
					}else {
						entityIDs.add(String.valueOf(getData.get("entityId")));
					}
					if(roleIDs.size() != 0) {
						if(!roleIDs.contains(String.valueOf(getData.get("roleID")))) {
							roleIDs.add(String.valueOf(getData.get("roleID")));
						}
					}else {
						roleIDs.add(String.valueOf(getData.get("roleID")));
					}
					if(entityTypeIDs.size() != 0) {
						if(!entityTypeIDs.contains(String.valueOf(getData.get("typeID")))) {
							entityTypeIDs.add(String.valueOf(getData.get("typeID")));
						}
					}else {
						entityTypeIDs.add(String.valueOf(getData.get("typeID")));
					}
					
					if(parentIDs.size() != 0) {
						if(!parentIDs.contains(Integer.parseInt(String.valueOf(getData.get("parentID"))))) {
							parentIDs.add(String.valueOf(getData.get("parentID")));
						}
					}else {
						parentIDs.add(String.valueOf(getData.get("parentID")));
					}
					
				}	
			}
			result.put("entityParentID",parentIDs);
			result.put("entityTypeID",entityTypeIDs);
			result.put("roleID",roleIDs);
			result.put("entityId",entityIDs);
			result.put("userID",userID);
		}
		return result;
		
	}
	
	
	public HashMap<String, Object> getSubItemRightsOfUserByItem(Integer userID,String token,List<String> itemTypeList,List<String> itemID,Integer itemParentID) {
		HashMap<String, Object> result = new HashMap<String,Object>();
		HashMap<String, Object> getUser = setDataForItemRightsOfUser(userID, token, itemID, itemTypeList, itemParentID);
		List entityParentList = (List) getUser.get("entityParentID");
		List entityIdsList = (List) getUser.get("entityId");
		List roleIDsList = (List) getUser.get("roleID");
		List typeIdsList = (List) getUser.get("entityTypeID");
		int loginUserId = (int) getUser.get("loginUserID");
		int UserId = (int) getUser.get("userID");
		String itemIDs = (String) getUser.get("itemID");
		String itemTypeIds = (String) getUser.get("itemTypeList")!=null?(String) getUser.get("itemTypeList"):"";
		
		String itemParentIds = String.valueOf(getUser.get("itemParentID"));
		
		String value = String.join(",", entityIdsList);
		
		List getRights = getRightsDetail(itemParentIds,itemTypeIds!="null"?itemTypeIds:null , typeIdsList.stream().collect(Collectors.joining(",")).toString(),entityParentList.stream().collect(Collectors.joining(",")).toString() , entityIdsList.stream().collect(Collectors.joining(",")).toString(), roleIDsList.stream().collect(Collectors.joining(",")).toString(), loginUserId, UserId);
		result.put("Content",getRights);
		return result;
	}
	
}


