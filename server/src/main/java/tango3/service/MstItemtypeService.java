package Tango3.service;

import org.springframework.stereotype.Service;

import Tango3.model.MstItem;
import Tango3.model.MstItemtype;

@Service
public class MstItemtypeService extends GenericService<MstItemtype> {
	
}
