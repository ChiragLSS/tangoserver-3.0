package Tango3.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.MstEntity;
import Tango3.repository.EntitymembermappingRepository;
import Tango3.repository.MstEntityRepository;
import Tango3.util.UtilConstants;

@Service
public class MstEntityService extends GenericService<MstEntity> {
	
	@Autowired
	private MstEntityRepository mstEntityRepository;
	@Autowired
	private EntitymembermappingService entitymembermappingService;
	@Autowired
	private EntitymembermappingRepository entitymembermappingRepository;
	
	
	public MstEntity findByID(int id) {
		return mstEntityRepository.findById(id);
	}
	
	public List<MstEntity> getByTypeID(int typeID){
		List<MstEntity> result = new ArrayList<MstEntity>();
		if(typeID != -1) {
			result = mstEntityRepository.getByTypeID(typeID);
		}else {
			result = mstEntityRepository.get();
		}
		return result;
		
	}
	
	public HashMap<String, Object> getChildList(int entityID,int level){
		HashMap<String, Object> result = new HashMap<String,Object>();
		
			List<HashMap<String, String>> child =  mstEntityRepository.getChildList(entityID);
			
			if(child.size() != 0) {
				 
					for (HashMap<String, String> childObject : child) {
						String id = String.valueOf(childObject.get("EntityID"));
						HashMap<String, Object> sub = getChildList(Integer.parseInt(id),level > 0 ? level : level - 1);
						HashMap<String, Object> member = entitymembermappingService.getByEntityID(Integer.parseInt(id));
						sub.putAll(member);
						result.put(childObject.get("EntityName"), arrange(childObject, sub));	
					}
				
			}else {
				HashMap<String, Object> member = entitymembermappingService.getByEntityID(entityID);
				result.putAll(member);
			}
			
		return result;
	}
	
	public HashMap<String, Object> arrange (HashMap<String, String> stringObject,HashMap<String, Object> obj) {
		HashMap<String, Object> result = new HashMap<String,Object>();
		result.putAll(stringObject);
		result.putAll(obj);
		return result;
	}
	
	public HashMap<String, Object> getParentList(int entityID,int entityTypeId,int level){
		HashMap<String, Object> result = new HashMap<String,Object>();
		
			List<HashMap<String, String>> child = new ArrayList<HashMap<String,String>>();
			
			if(entityTypeId < 5) {
				child =	mstEntityRepository.getParentList(entityID);
			}else {
				child = entitymembermappingRepository.getParentDataByUserID(entityID);
			}
			if(child.size() != 0) {
					for (HashMap<String, String> childObject : child) {
						String id = String.valueOf(childObject.get("ParentEntityID"));
						String typeID = String.valueOf(childObject.get("ParentEntityTypeID"));
						HashMap<String, Object> sub = getParentList(Integer.parseInt(id),Integer.parseInt(typeID),level > 0 ? level :level - 1);
						HashMap<String, Object> member = entitymembermappingService.getParentList(Integer.parseInt(id));
						sub.putAll(member);
						result.put(childObject.get("ParentEntityName"), arrange(childObject, sub));	
					}
			}
		
		return result;
	}
	
	public HashMap<String, Object> getBelowLevelMember(int userID) {
		List getEntityIDs = entitymembermappingService.getBelongToEntityIDsByUserID(userID);
		HashMap<String, Object> result = new HashMap<String,Object>();
		List<HashMap<String, String>> belowMember = new ArrayList<HashMap<String,String>>();
		for(Object obj : getEntityIDs)  {
			HashMap<String,String> getObject = (HashMap<String, String>) obj;
			HashMap<String, Object> getMemberData = getBelowMember(Integer.parseInt(String.valueOf(getObject.get("EntityID"))));
			for(String keys : getMemberData.keySet()) {
				Object data = getMemberData.get(keys);
				if(data.getClass() == ArrayList.class) {
					List belowList = (List) data;
					for(Object get : belowList) {
						HashMap<String, String> getBelow = (HashMap<String, String>) get;
						belowMember.add(getBelow);
					}
				}	
			}
		}
		result.put("Members",belowMember);
		return result;
	}
	
	public HashMap<String, Object> getBelowLevelMemberByEntityID(int entityID) {
		HashMap<String, Object> result = new HashMap<String,Object>();
		List<HashMap<String, String>> belowMember = new ArrayList<HashMap<String,String>>();
//			HashMap<String,String> getObject = (HashMap<String, String>) obj;
			HashMap<String, Object> getMemberData = getBelowMember(entityID);
			for(String keys : getMemberData.keySet()) {
				Object data = getMemberData.get(keys);
				if(data.getClass() == ArrayList.class) {
					List belowList = (List) data;
					for(Object get : belowList) {
						HashMap<String, String> getBelow = (HashMap<String, String>) get;
						belowMember.add(getBelow);
					}
				}	
			}
		result.put("Members",belowMember);
		return result;
	}
	
	public HashMap<String, Object> getBelowMember(int userID){
	   HashMap<String, Object> result = new HashMap<String,Object>();
	   List<HashMap<String, String>> child =  mstEntityRepository.getChildList(userID);
		if(child.size() != 0) {
			for (HashMap<String, String> childObject : child) {
				String id = String.valueOf(childObject.get("EntityID"));
				HashMap<String, Object> sub = getBelowMember(Integer.parseInt(id));
				HashMap<String, Object> member = entitymembermappingService.getMemberByEntityID(Integer.parseInt(id));
				for(String keys : member.keySet()) {
					List obj = (List) member.get(keys);
					for(Object get : obj) {
						HashMap<String, String> getBelow = (HashMap<String, String>) get;
						if(getBelow.get("EntityRoleName") != "Member") {
							result.putAll(getBelow);
						}
					}
				}
			result.putAll(sub); 
			}
		}else {
			HashMap<String, Object> member = entitymembermappingService.getMemberByEntityID(userID);
			result.putAll(member);
		}
		
		return result;
	}
	
	public MstEntity checkEntityExist(int entityID,int typeID,byte active) {
		return mstEntityRepository.checkEntityExist(entityID,typeID,active);
	}
	
	public HashMap<String, Object> belongsTo(int userID){
		HashMap<String, Object> result = new HashMap<String,Object>();
		List getEntityIDs = entitymembermappingService.getBelongToEntityIDsByUserID(userID);
		for(Object s : getEntityIDs) {
			HashMap<String, String> getData = (HashMap<String, String>) s;
			result.put(getData.get("EntityName"),getChildList(Integer.parseInt(String.valueOf(getData.get("EntityID"))), -1));
		}
	return result;
	}
	public HashMap<String, Object> reportsTo(int userID,int typeID){
		HashMap<String, Object> result = new HashMap<String,Object>();
		List getEntityIDs = null;
		if(typeID != UtilConstants.getEntityType.get("Member")) {
			getEntityIDs = entitymembermappingService.getReportsToEntityIDByUserID(userID);
		}else {
			getEntityIDs = entitymembermappingService.getBelongToEntityIDsByUserID(userID);
		}
		for(Object s : getEntityIDs) {
			HashMap<String, String> getData = (HashMap<String, String>) s;
			HashMap< String, Object> reportsTo = entitymembermappingService.getParentList(Integer.parseInt(String.valueOf(getData.get("EntityID"))));
			if(reportsTo.size() != 0) {
				result.put(getData.get("EntityName"),reportsTo);
			}
		}
		return result;
	}
	
	public int getEntityIDByEntityName(String entityName) {
		return mstEntityRepository.getEntityIdByEntityName(entityName);
	}
	
	
	public List getMemberListFromEntityList(int userID,int entityID) {
		List getEntityIDs = entitymembermappingService.getBelongToEntityIDsByUserID(userID);
		HashMap<String, Integer> storeIdsForDublicate = new HashMap<String,Integer>();
		List<HashMap<String, String>> belowMember = new ArrayList<HashMap<String,String>>();
		List result = new ArrayList<>();
		if(entityID == -1) {
			for(Object obj : getEntityIDs)  {
				HashMap<String,String> getObject = (HashMap<String, String>) obj;
				List getMemberData = getMemberListFromEntityIDs(Integer.parseInt(String.valueOf(getObject.get("EntityID"))));
				for(Object get : getMemberData) {
					if(get.getClass() == ArrayList.class) {
						List objList = (List) get;
						for(Object objArray : objList) {
							HashMap<String, String> getBelow = (HashMap<String, String>) objArray;
							if(storeIdsForDublicate.get(String.valueOf(getBelow.get("entityName"))) == null) {
								belowMember.add(getBelow);
								storeIdsForDublicate.put(String.valueOf(getBelow.get("entityName")),Integer.parseInt(String.valueOf(getBelow.get("userID"))));
							}
						}
					}else {
						HashMap<String, String> getBelow = (HashMap<String, String>) get;
						if(storeIdsForDublicate.get(String.valueOf(getBelow.get("entityName"))) == null) {
							belowMember.add(getBelow);
							storeIdsForDublicate.put(String.valueOf(getBelow.get("entityName")),Integer.parseInt(String.valueOf(getBelow.get("userID"))));
						}
					}
				}
			}
		}else {
			
			List getMemberData = getMemberListFromEntityIDs(entityID);
			for(Object get : getMemberData) {
				if(get.getClass() == HashMap.class) {
					HashMap<String, String> getBelow = (HashMap<String, String>) get;
					if(storeIdsForDublicate.get(String.valueOf(getBelow.get("entityName"))) == null) {
						belowMember.add(getBelow);
						storeIdsForDublicate.put(String.valueOf(getBelow.get("entityName")),Integer.parseInt(String.valueOf(getBelow.get("userID"))));
					}
				}else {
					List getList = (List) get;
					for(Object getObj : getList) {
						HashMap<String, String> getBelow = (HashMap<String, String>) getObj;
						if(storeIdsForDublicate.get(String.valueOf(getBelow.get("entityName"))) == null) {
							belowMember.add(getBelow);
							storeIdsForDublicate.put(String.valueOf(getBelow.get("entityName")),Integer.parseInt(String.valueOf(getBelow.get("userID"))));
						}
					}	
				}
			}
			
		}
		result.addAll(belowMember);
		return result;
	}
	
	
	
	private List getMemberListFromEntityIDs(int userID){
		   List result = new ArrayList<>();
		   List<HashMap<String, String>> child =  mstEntityRepository.getChildList(userID);
			if(child.size() != 0) {
				for (HashMap<String, String> childObject : child) {
					String id = String.valueOf(childObject.get("EntityID"));
					List sub = getMemberListFromEntityIDs(Integer.parseInt(id));
					HashMap<String, Object> member = entitymembermappingService.getByEntityID(Integer.parseInt(id));
					for(String keys : member.keySet()) {
						List obj = (List) member.get(keys);
						for(Object get : obj) {
							HashMap<String, String> getBelow = (HashMap<String, String>) get;
							HashMap<String, String> tempList = new HashMap<String,String>();
							tempList.put("userID", String.valueOf(getBelow.get("EntityUserID")));
							tempList.put("entityName", getBelow.get("userName"));
							result.add(tempList);
						}
					}
				result.add(sub);
				}
			}else {
				HashMap<String, Object> member = entitymembermappingService.getByEntityID(userID);
				for(String keys : member.keySet()) {
					List obj = (List) member.get(keys);
					for(Object get : obj) {
						HashMap<String, String> getBelow = (HashMap<String, String>) get;
						HashMap<String, String> tempList = new HashMap<String,String>();
						tempList.put("userID", String.valueOf(getBelow.get("EntityUserID")));
						tempList.put("entityName", getBelow.get("userName"));
						result.add(tempList);
					}
				}
			}
			return result;
		}
	
	
}
