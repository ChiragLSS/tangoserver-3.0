package Tango3.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.MstUserdetail;
import Tango3.repository.MstUserdetailRepository;
import Tango3.util.UtilConstants;

@Service
public class MstUserdetailService extends GenericService<MstUserdetail> {
	
	@Autowired
	private MstUserdetailRepository userdetailRepository;
	@Autowired
	private MstEntityService mstEntityService;
	@Autowired
	private MstMemberService mstMemberService;
	
	public List getAllUserDetail() {
		return userdetailRepository.getAllUserDetail();
	}
	
	public List ActiveUsers() {
		return userdetailRepository.activeUsers();
	}
	
	public MstUserdetail findByID(int userID) {
		return userdetailRepository.findByUserID(userID);
	}
	
	public List getDegreeListForNewUser() {
		return userdetailRepository.getDegreeListForNewUser();
	}
	
	
	public List getBranchListForNewUser() {
		return userdetailRepository.getBranchListForNewUser();
	}
	
	public boolean checkUserNameIsExistsForNewUser(String wikiName) {
		List mu = userdetailRepository.checkUserNameisExistsForNewUser(wikiName);
		if(mu.size() != 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public List getListOfAllUsers() {
		return userdetailRepository.listOFAllUsers();
	}
	
	public List getUserProfileSummary(int userID) {
		return userdetailRepository.getUserProfileSummary(userID);
	}
	
	public List getUserPersonalDetail(int userID) {
		return userdetailRepository.getUserPersonalDetail(userID);
	}
	
	public List getUserProfilePicWithBasicDetails(int userID) {
		return userdetailRepository.getUserProfilePicWithBasicDetails(userID);
	}
	
	public String getUserNameFromUserID(int userID) {
		return userdetailRepository.getUserNameFromUserID(userID);
	}
	
	public byte isUserGm(int userID) {
		return userdetailRepository.isUserGM(userID);
	}
	
	public byte isSuperUser(int userID) {
		return userdetailRepository.isUserGM(userID);
	}
	
	public int getUserIdFromWikiName(String wikiName) {
		return userdetailRepository.getUserIDFromWikiName(wikiName);
	}
	
	public int updateLockerDetail(int userID,String lockerNum,String keyNum) {
		MstUserdetail userDetail = findByID(userID);
		userDetail.setKeyNum(keyNum);
		userDetail.setLockerNum(lockerNum);
		this.save(userDetail);
		
		return userDetail.getUserID()!=0?1:0; 
	}
	
	public int updateAccessCardNumber(int userID,String AccessCardNumber) {
		MstUserdetail userDetail = findByID(userID);
		userDetail.setAccessCardNo(AccessCardNumber);
		this.save(userDetail);
		
		return userDetail.getUserID()!=0?1:0; 
	}
	
	
	public int updateMobileAndTokenNumber(int userID,String TokenNumber,String MobileNumber) {
		MstUserdetail userDetail = findByID(userID);
		userDetail.setTokenNumberAttachedToFb(TokenNumber);
		userDetail.setMobileNumberAttachedToFb(MobileNumber);
		this.save(userDetail);
		
		return userDetail.getUserID()!=0?1:0; 
	}
	
	public String getLeavingDate(int userID) {
		return userdetailRepository.getLeavingDate(userID);
	}
	
	public String getEmployementType(int userID) {
		return userdetailRepository.getEmployementType(userID);
	}
	
	public int updateUserExit(int userID) {
		MstUserdetail userDetail = findByID(userID);
		userDetail.setIsActive((byte)0);
		userDetail.setEmployeeStatus(UtilConstants.getEmployeeStatus.get("PAST_EMPLOYEE"));
		this.save(userDetail);
		
		return userDetail.getUserID()!=0?1:0; 
	}	
	
	public MstUserdetail checkUserExist(String firstName,String lastName) {
		
		List<MstUserdetail> user =userdetailRepository.checkUserExist(firstName,lastName); 
		
		return user.size() != 0?user.get(0):null;
	}
	
	public int setUserLeavingDate(int userID) {
		MstUserdetail user = findByID(userID);
		user.setLeavingDate(new Date());
		user.setEmployeeStatus(UtilConstants.getEmployeeStatus.get("PENDING_EXIT"));
		this.save(user);
		return user.getUserID();
	}
	
	public int updateEmployeeStatus(int userID,String status) {
		MstUserdetail user = findByID(userID);
		user.setLeavingDate(null);
		user.setEmployeeStatus(status);
		this.save(user);
		return user.getUserID();
	}
	
	public MstUserdetail getUserDetailFromEmail(String email) {
		List<MstUserdetail> user = userdetailRepository.getUserDetailFromEmail(email);
		return user.get(0); 
	}
	
	
	public MstUserdetail changeToEmployee(String employeementDate,String reason,int userID) {
		MstUserdetail user = findByID(userID);
		user.setEmployementType("Employee");
		user.setEmployeementDate(new Date(employeementDate));
		user.setEmploymentReason(reason);
		this.save(user);
		return user;
	}
	
	public HashMap<String, Object> getUserInformation(int userID) {
		HashMap<String, Object> result = new HashMap<String,Object>();
		HashMap<String, Object> reportsTo = mstEntityService.reportsTo(userID, 5);
		HashMap<String, Object> BelongsTo = mstEntityService.belongsTo(userID);
		result.put("reportsTo", reportsTo);
		result.put("belongsTo", BelongsTo);
		return result;
	}
	
	
	public List getNeccessaryUserDocumentList() {
		List result = new ArrayList<>();
		HashMap<String, Object> mandatoryList = new HashMap<String,Object>();
		List mandatory = new ArrayList<>();
		mandatory.add("Lodestone Offer Letter Accepted");
		mandatory.add("Resume");
		mandatory.add("Pan Card");
		mandatory.add("2 Passport Size Photos");
		mandatoryList.put("area", "Mandatory");
		mandatoryList.put("field", mandatory);
		
		HashMap<String, Object> PhotoIDList = new HashMap<String,Object>();
		List PhotoID = new ArrayList<>();
		PhotoID.add("Driving License");
		PhotoID.add("Passport");
		PhotoIDList.put("area", "Photo Id Either Of");
		PhotoIDList.put("field", PhotoID);
		
		HashMap<String, Object> AddressList = new HashMap<String,Object>();
		List Address = new ArrayList<>();
		Address.add("Telephone Bill");
		Address.add("Electricity Bill");
		Address.add("Aadhar Card");
		Address.add("Election Card");
		Address.add("Ration Card");
		Address.add("Rental Agreement");
		AddressList.put("area", "Address Proof (permanent/local) Either Of");
		AddressList.put("field", Address);
		
		
		HashMap<String, Object> PreviousList = new HashMap<String,Object>();
		List Previous = new ArrayList<>();
		Previous.add("Appointment Letter");
		Previous.add("3 Months Salary Slips");
		Previous.add("Resignation Letter");
		Previous.add("Reliving Letter");
		Previous.add("Experience Letter");
		PreviousList.put("area", "Previous Organization");
		PreviousList.put("field", Previous);
		
		result.add(mandatoryList);
		result.add(PhotoIDList);
		result.add(AddressList);
		result.add(PreviousList);
		return result;
	}
	
	public HashMap<String, Object> getMarksheetCertificateList() {
		HashMap<String, Object> result = new HashMap<String,Object>();
		
		HashMap<String, Object> markSheets = new HashMap<String,Object>();
		List markSheet = new ArrayList<>();
		markSheet.add("S.S.C.");
		markSheet.add("H.S.C.");
		markSheet.add("Bachelor");
		markSheet.add("Master");
		markSheets.put("area", "MarkSheets");
		markSheets.put("field", markSheet);
		
		HashMap<String, Object> degreeList = new HashMap<String,Object>();
		List degree = new ArrayList<>();
		degree.add("S.S.C.");
		degree.add("H.S.C.");
		degree.add("Bachelor");
		degree.add("Master");
		degree.add("IPC C.A. Inter");
		degree.add("Diploma");
		degreeList.put("area", "Degree Certificates");
		degreeList.put("field", degree);
		
		List merge =new ArrayList<>(); 
		merge.add(markSheets);
		merge.add(degreeList);
		
		result.put("data",merge);
		result.put("UserMarksheetReturnDate",UtilConstants.UserMarksheetReturnDate);
		return result;
	}
	
	public boolean iValidRequest(int userID,int loginUserID) {
		List userIDs = mstMemberService.getMemberdetail(loginUserID);
		for(Object userid :  userIDs) {
			if(userid.getClass() == HashMap.class) {
				HashMap<String, String> getObj = (HashMap<String, String>) userid;
				if(Integer.parseInt(String.valueOf(getObj.get("userID"))) == userID) {
					return true;
				}
			}else if(userid.getClass() == MstUserdetail.class) {
				MstUserdetail mstUserdetail = (MstUserdetail)userid;
				if(mstUserdetail.getUserID() == userID) {
					return true;
				}
			}
		}
		return false;
	}
	
	
}
