package Tango3.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Tango3.model.Itemright;
import Tango3.repository.ItemrightRepository;

@Service
public class ItemrightService extends GenericService<Itemright> {

	@Autowired
	private ItemrightRepository itemrightRepository;
	
	public List getRightofItem() {
		return itemrightRepository.getRightsOfItems();
	}

	
	public List getRightsofProfile() {
		return itemrightRepository.getRightsOfProfile();
	}
	
	public List getRightsofProfileItem() {
		List result = new ArrayList<>();
		List profileItems =  itemrightRepository.getRightsofProfileItem();
		List items = itemrightRepository.getRightsOfItems();
		result.addAll(profileItems);
		result.addAll(items);
		return result;
	}
}
